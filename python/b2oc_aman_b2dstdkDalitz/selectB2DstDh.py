def configure_b2oc_aman_dstdkDalitz_selection( locationRoot = "" , dataYear = "" , isSimulation = False ) :
  from PhysSelPython.Wrappers import AutomaticData, SelectionSequence, Selection, MergedSelection
  import sys

  # ================================
  # Prep trigger and stripping list
  # ================================

  mystrips   = ["StrippingB2DstDKBeauty2CharmLineDecision", "StrippingB2DstDKDstarD02K3PiBeauty2CharmLineDecision",
                "StrippingB02DstD0KDstarD02K3PiBeauty2CharmLineDecision","StrippingB02DstD0KD02K3PiBeauty2CharmLineDecision","StrippingB02DstD0KBeauty2CharmLineDecision",
                "StrippingB2D0D0KD02HHD02HHBeauty2CharmLineDecision","StrippingB2Dst0D0KDst02D0GammaD02HHBeauty2CharmLineDecision","StrippingB2Dst0D0KDst02D0Pi0ResolvedD02HHBeauty2CharmLineDecision",
                "StrippingB2D0D0KD02HHD02K3PiBeauty2CharmLineDecision","StrippingB02D0DKBeauty2CharmLineDecision",
                "StrippingB02D0DKD02K3PiBeauty2CharmLineDecision","StrippingB2D0D0KD02K3PiD02K3PiBeauty2CharmLineDecision",
                "StrippingB2DstDPiBeauty2CharmLineDecision",
                "StrippingB2DstDPiDstarD02K3PiBeauty2CharmLineDecision"]

  mytriggers = ["L0DiMuonDecision","L0ElectronDecision","L0ElectronHiDecision",
                "L0MuonDecision","L0HadronNoSPDDecision","L0MUON,minbiasDecision",
                "L0PhotonDecision","L0HadronDecision","L0MuonNoSPDDecision",
                "Hlt1TrackAllL0Decision","Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision",
                "Hlt2Topo2BodyBBDTDecision","Hlt2Topo3BodyBBDTDecision","Hlt2Topo4BodyBBDTDecision","Hlt2IncPhiDecision","Hlt2PhiIncPhiDecision",
                "Hlt2Topo2BodyDecision","Hlt2Topo3BodyDecision","Hlt2Topo4BodyDecision"]

  #===================================#
  #=== Prepare PID substituters ======#
  #===================================#
  from Configurables import SubstitutePID
    # Bachelor kaon
  def doBachPIDswap(source,target):
    subBachKtoPi = SubstitutePID("subBachKtoPi",
                               Code="DECTREE('Beauty -> Xc Xc Xs')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> Xc Xc ^K-':'pi-',
                                                 'Beauty -> Xc Xc ^K+':'pi+'}
                               );
    newSel = Selection("sel"+target,Algorithm=subBachKtoPi,RequiredSelections=[sel[source]]);
    return newSel;
    # B- -> D+ (=> Ds+) X (with bachelor pi/K)
  def doPIDswap_Bm2DpX_to_Bm2DspX(source,target):
    sub_Bm2DpX_to_Bm2DspX = SubstitutePID("sub_Bm2DpX_to_Bm2DspX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> ^D+ Xc Meson':'D_s+',
                                                'B+ -> ^D- Xc Meson':'D_s-'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_Bm2DpX_to_Bm2DspX,RequiredSelections=[sel[source]]);
    return newSel;
    # B- -> D- (=> Ds-) X (with bachelor pi/K)
  def doPIDswap_Bm2DmX_to_Bm2DsmX(source,target):
    sub_Bm2DmX_to_Bm2DsmX = SubstitutePID("sub_Bm2DmX_to_Bm2DsmX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> ^D- Xc Meson':'D_s-',
                                                'B+ -> ^D+ Xc Meson':'D_s+'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_Bm2DmX_to_Bm2DsmX,RequiredSelections=[sel[source]]);
    return newSel;
    # B- -> D+ (=> Ds+) D (with bachelor pi/K)
  def doPIDswap_Bm2DpDmX_to_Bm2DspDmX(source,target):
    sub_Bm2DpDmX_to_Bm2DspDmX = SubstitutePID("sub_Bm2DpDmX_to_Bm2DspDmX",
                               Code="DECTREE('Beauty -> D+ D- Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> ^D+ Xc Meson':'D_s+',
                                                 'B+ -> ^D- Xc Meson':'D_s-'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_Bm2DpDmX_to_Bm2DspDmX,RequiredSelections=[sel[source]]);
    return newSel;
    # B- -> D+ D (=>Ds-) (with bachelor pi/K)
  def doPIDswap_Bm2DpDmX_to_Bm2DpDsmX(source,target):
    sub_Bm2DpDmX_to_Bm2DpDsmX = SubstitutePID("sub_Bm2DpDmX_to_Bm2DpDsmX",
                               Code="DECTREE('Beauty -> D+ D- Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> Xc ^D- Meson':'D_s-',
                                                 'B+ -> Xc ^D+ Meson':'D_s+'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_Bm2DpDmX_to_Bm2DpDsmX,RequiredSelections=[sel[source]]);
    return newSel;
    # B- -> D- D (=>Ds-) (with bachelor pi/K)
  def doPIDswap_Bm2DDmX_to_Bm2DDsmX(source,target):
    sub_Bm2DDmX_to_Bm2DDsmX = SubstitutePID("sub_Bm2DDmX_to_Bm2DDsmX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> Xc ^D- Meson':'D_s-',
                                                 'B+ -> Xc ^D+ Meson':'D_s+'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_Bm2DDmX_to_Bm2DDsmX,RequiredSelections=[sel[source]]);
    return newSel;
    # B0 -> D+ (=> Ds+) D0bar X- (just look for the D which isn't a D0)
  def doPIDswap_B02DpD0barX_to_B02DspD0barX(source,target):
    sub_B02DpD0barX_to_B02DspD0barX = SubstitutePID("sub_B02DpD0barX_to_B02DspD0barX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> ^D+ Xc Meson':'D_s+',
                                                 'Beauty -> ^D- Xc Meson':'D_s-'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_B02DpD0barX_to_B02DspD0barX,RequiredSelections=[sel[source]]);
    return newSel;
  # B0 -> D+ (=> Ds+) D(*)- KS (just change one D D+ or D-)
  def doPIDswap_B02DDX_to_B02DsDX(source,target):
    sub_B02DpDmX_to_B02DspDmX = SubstitutePID("sub_B02DpDmX_to_B02DspDmX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> ^D+ Xc Meson':'D_s+'}
                               );
    newSel1 = Selection("sel1"+target,Algorithm=sub_B02DpDmX_to_B02DspDmX,RequiredSelections=[sel[source]]);
    sub_B02DpDmX_to_B02DpDsmX = SubstitutePID("sub_B02DpDmX_to_B02DsDsmX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> Xc ^D- Meson':'D_s-'}
                               );
    newSel2 = Selection("sel2"+target,Algorithm=sub_B02DpDmX_to_B02DpDsmX,RequiredSelections=[sel[source]]);
    return MergedSelection("sel"+target, RequiredSelections=[newSel1,newSel2]);


  ########################################
  # Get candidates from their stripping
  # location
  ########################################
  # Selections
  sel={}

  sel["BmD0D0barKm"]           = AutomaticData(Location = locationRoot+"Phys/B2D0D0KD02HHD02HHBeauty2CharmLine/Particles");
  sel["BmD0_K3piD0bar_K3piKm"] = AutomaticData(Location = locationRoot+"Phys/B2D0D0KD02K3PiD02K3PiBeauty2CharmLine/Particles");
  sel["BmD0_K3piD0barKm"]      = AutomaticData(Location = locationRoot+"Phys/B2D0D0KD02HHD02K3PiBeauty2CharmLine/Particles");
  sel["BmD0D0bar_K3piKm"]      = AutomaticData(Location = locationRoot+"Phys/B2D0D0KD02HHD02K3PiBeauty2CharmLine/Particles");

  sel["B0DmD0Kp"]              = AutomaticData(Location = locationRoot+"Phys/B02D0DKBeauty2CharmLine/Particles");
  sel["B0DmD0_K3piKp"]         = AutomaticData(Location = locationRoot+"Phys/B02D0DKD02K3PiBeauty2CharmLine/Particles");

  # Dstar
  sel["BmDstpDmKm"]                = AutomaticData(Location = locationRoot+"Phys/B2DstDKBeauty2CharmLine/Particles"); 
  sel["BmDstmDpKm"]=sel["BmDstpDmKm"]; 
  sel["B0DstmD0Kp"]                = AutomaticData(Location = locationRoot+"Phys/B02DstD0KBeauty2CharmLine/Particles"); 
  sel["B0DstpD0barKm"]=sel["B0DstmD0Kp"]; 
  sel["B0Dstm_K3piD0Kp"]          = AutomaticData(Location = locationRoot+"Phys/B02DstD0KDstarD02K3PiBeauty2CharmLine/Particles"); 
  sel["B0DstmD0_K3piKp"]          = AutomaticData(Location = locationRoot+"Phys/B02DstD0KD02K3PiBeauty2CharmLine/Particles"); 
    
  sel["BmDstp_K3piDmKm"]           = AutomaticData(Location = locationRoot+"Phys/B2DstDKDstarD02K3PiBeauty2CharmLine/Particles"); 
  sel["BmDstm_K3piDpKm"]           = AutomaticData(Location = locationRoot+"Phys/B2DstDKDstarD02K3PiBeauty2CharmLine/Particles"); 

  sel["BmDst0_gammaD0barKmS28"]    = AutomaticData(Location = locationRoot+"Phys/B2Dst0D0KDst02D0GammaD02HHBeauty2CharmLine/Particles");
  sel["BmDst0bar_gammaD0KmS28"]    = sel["BmDst0_gammaD0barKmS28"]; 

  sel["BmDst0_pi0D0barKmS28"]      = AutomaticData(Location = locationRoot+"Phys/B2Dst0D0KDst02D0Pi0ResolvedD02HHBeauty2CharmLine/Particles"); 
  sel["BmDst0bar_pi0D0KmS28"]      = sel["BmDst0_pi0D0barKmS28"]; 
  
  sel["BmDstmDmKp"]                = AutomaticData(Location = locationRoot+"Phys/B2DstDKBeauty2CharmLine/Particles"); 

  sel["BmDstpDmpim"]                = AutomaticData(Location = locationRoot+"Phys/B2DstDPiBeauty2CharmLine/Particles"); 
  sel["BmDstmDppim"]=sel["BmDstpDmpim"]; 
  sel["BmDstmDmpip"]                = AutomaticData(Location = locationRoot+"Phys/B2DstDPiBeauty2CharmLine/Particles"); 
  
  sel["BmDstm_K3piDmKp"]           = AutomaticData(Location = locationRoot+"Phys/B2DstDKDstarD02K3PiBeauty2CharmLine/Particles"); 

  sel["BmDstp_K3piDmpim"]           = AutomaticData(Location = locationRoot+"Phys/B2DstDPiDstarD02K3PiBeauty2CharmLine/Particles"); 
  sel["BmDstm_K3piDppim"]           = AutomaticData(Location = locationRoot+"Phys/B2DstDPiDstarD02K3PiBeauty2CharmLine/Particles"); 
  sel["BmDstm_K3piDmpip"]           = AutomaticData(Location = locationRoot+"Phys/B2DstDPiDstarD02K3PiBeauty2CharmLine/Particles"); 
  
  # Dstar and Ds
  
  sel["BmDstpDsmKm"]               =doPIDswap_Bm2DmX_to_Bm2DsmX("BmDstpDmKm","BmDstpDsmKm");
  sel["BmDstmDspKm"]               =doPIDswap_Bm2DpX_to_Bm2DspX("BmDstmDpKm","BmDstmDspKm");

  sel["BmDstpDsmpim"]               =doPIDswap_Bm2DmX_to_Bm2DsmX("BmDstpDmpim","BmDstpDsmpim");
  sel["BmDstmDsppim"]               =doPIDswap_Bm2DpX_to_Bm2DspX("BmDstmDppim","BmDstmDsppim");

  sel["BmDstm_K3piDspKm"]           =doPIDswap_Bm2DpX_to_Bm2DspX("BmDstm_K3piDpKm","BmDstm_K3piDspKm");
  sel["BmDstp_K3piDsmKm"]           =doPIDswap_Bm2DmX_to_Bm2DsmX("BmDstp_K3piDmKm","BmDstp_K3piDsmKm");

  sel["BmDstm_K3piDsppim"]           =doPIDswap_Bm2DpX_to_Bm2DspX("BmDstm_K3piDppim","BmDstm_K3piDsppim");
  sel["BmDstp_K3piDsmpim"]           =doPIDswap_Bm2DmX_to_Bm2DsmX("BmDstp_K3piDmpim","BmDstp_K3piDsmpim");
  seq={}
  seqList=[] # to put in the sequence
  for s in sel.keys():
    seq[s] = SelectionSequence("seq_"+s, TopSelection = sel[s] );
    seqList += [seq[s].sequence()]

  ########################################
  # Useful DTT functions
  ########################################
  def getDbranchesDict(_0, pre, d_string, _2):
    if(d_string=="D0"):          return {pre      : _0+"^( [D0]cc -> K- pi+ )"+_2, 
                                         pre+"_K" : _0+"( [D0]cc -> ^K- pi+ )"+_2, 
                                         pre+"_pi": _0+"( [D0]cc -> K- ^pi+ )"+_2
                                         }
    elif(d_string=="D0bar"):     return {pre      : _0+"^( [D0]cc -> K+ pi- )"+_2, 
                                         pre+"_K" : _0+"( [D0]cc -> ^K+ pi- )"+_2, 
                                         pre+"_pi": _0+"( [D0]cc -> K+ ^pi- )"+_2
                                         }
    if(d_string=="D0_K3pi"):     return {pre       : _0+"^( [D0]cc -> K- pi+ pi+ pi- )"+_2, 
                                         pre+"_K"  : _0+"( [D0]cc -> ^K- pi+ pi+ pi- )"+_2, 
                                         pre+"_pi1": _0+"( [D0]cc -> K- ^pi+ pi+ pi- )"+_2, 
                                         pre+"_pi2": _0+"( [D0]cc -> K- pi+ ^pi+ pi- )"+_2, 
                                         pre+"_pi3": _0+"( [D0]cc -> K- pi+ pi+ ^pi- )"+_2
                                         }
    elif(d_string=="D0bar_K3pi"):return {pre       : _0+"^( [D0]cc -> K+ pi- pi- pi+ )"+_2, 
                                         pre+"_K"  : _0+"( [D0]cc -> ^K+ pi- pi- pi+ )"+_2, 
                                         pre+"_pi1": _0+"( [D0]cc -> K+ ^pi- pi- pi+ )"+_2, 
                                         pre+"_pi2": _0+"( [D0]cc -> K+ pi- ^pi- pi+ )"+_2, 
                                         pre+"_pi3": _0+"( [D0]cc -> K+ pi- pi- ^pi+ )"+_2
                                         }
    if(d_string=="Dp"):          return {pre       : _0+"^( D+ -> K- pi+ pi+ )"+_2, 
                                         pre+"_K"  : _0+"( D+ -> ^K- pi+ pi+ )"+_2, 
                                         pre+"_pi1": _0+"( D+ -> K- ^pi+ pi+ )"+_2, 
                                         pre+"_pi2": _0+"( D+ -> K- pi+ ^pi+ )"+_2
                                         }
    if(d_string=="Dsp"):          return {pre     : _0+"^( D_s+ -> K- K+ pi+ )"+_2, 
                                         pre+"_K1": _0+"( D_s+ -> ^K- K+ pi+ )"+_2, 
                                         pre+"_K2": _0+"( D_s+ -> K- ^K+ pi+ )"+_2, 
                                         pre+"_pi": _0+"( D_s+ -> K- K+ ^pi+ )"+_2
                                         }
    if(d_string=="Dstp"):        return {pre        : _0+"^( D*(2010)+ -> ( [D0]cc -> K- pi+ ) pi+ )"+_2,         
                                         pre+"_pi"  : _0+"( D*(2010)+ -> ( [D0]cc -> K- pi+ ) ^pi+ )"+_2,          
                                         pre+"_D"   : _0+"( D*(2010)+ -> ^( [D0]cc -> K- pi+ ) pi+ )"+_2,        
                                         pre+"_D_K" : _0+"( D*(2010)+ -> ( [D0]cc -> ^K- pi+ ) pi+ )"+_2,       
                                         pre+"_D_pi": _0+"( D*(2010)+ -> ( [D0]cc -> K- ^pi+ ) pi+ )"+_2
                                         }
    if(d_string=="Dstp_K3pi"):   return {pre         : _0+"^( D*(2010)+ -> ( [D0]cc -> K- pi+ pi+ pi- ) pi+ )"+_2, 
                                         pre+"_pi"   : _0+"( D*(2010)+ -> ( [D0]cc -> K- pi+ pi+ pi- ) ^pi+ )"+_2,  
                                         pre+"_D"    : _0+"( D*(2010)+ -> ^( [D0]cc -> K- pi+ pi+ pi- ) pi+ )"+_2, 
                                         pre+"_D_K"  : _0+"( D*(2010)+ -> ( [D0]cc -> ^K- pi+ pi+ pi- ) pi+ )"+_2, 
                                         pre+"_D_pi1": _0+"( D*(2010)+ -> ( [D0]cc -> K- ^pi+ pi+ pi- ) pi+ )"+_2, 
                                         pre+"_D_pi2": _0+"( D*(2010)+ -> ( [D0]cc -> K- pi+ ^pi+ pi- ) pi+ )"+_2, 
                                         pre+"_D_pi3": _0+"( D*(2010)+ -> ( [D0]cc -> K- pi+ pi+ ^pi- ) pi+ )"+_2
                                         }
    if(d_string=="Dm"):          return {pre       : _0+"^( D- -> K+ pi- pi- )"+_2, 
                                         pre+"_K"  : _0+"( D- -> ^K+ pi- pi- )"+_2, 
                                         pre+"_pi1": _0+"( D- -> K+ ^pi- pi- )"+_2, 
                                         pre+"_pi2": _0+"( D- -> K+ pi- ^pi- )"+_2
                                         }
    if(d_string=="Dsm"):          return {pre     : _0+"^( D_s- -> K+ K- pi- )"+_2, 
                                         pre+"_K1": _0+"( D_s- -> ^K+ K- pi- )"+_2, 
                                         pre+"_K2": _0+"( D_s- -> K+ ^K- pi- )"+_2, 
                                         pre+"_pi": _0+"( D_s- -> K+ K- ^pi- )"+_2
                                         }
    if(d_string=="Dstm"):        return {pre        : _0+"^( D*(2010)- -> ( [D0]cc -> K+ pi- ) pi- )"+_2, 
                                         pre+"_pi"  : _0+"( D*(2010)- -> ( [D0]cc -> K+ pi- ) ^pi- )"+_2, 
                                         pre+"_D"   : _0+"( D*(2010)- -> ^( [D0]cc -> K+ pi- ) pi- )"+_2, 
                                         pre+"_D_K" : _0+"( D*(2010)- -> ( [D0]cc -> ^K+ pi- ) pi- )"+_2, 
                                         pre+"_D_pi": _0+"( D*(2010)- -> ( [D0]cc -> K+ ^pi- ) pi- )"+_2
                                         }
    if(d_string=="Dstm_K3pi"):   return {pre         : _0+"^( D*(2010)- -> ( [D0]cc -> K+ pi- pi- pi+ ) pi- )"+_2, 
                                         pre+"_pi"   : _0+"( D*(2010)- -> ( [D0]cc -> K+ pi- pi- pi+ ) ^pi- )"+_2,  
                                         pre+"_D"    : _0+"( D*(2010)- -> ^( [D0]cc -> K+ pi- pi- pi+ ) pi- )"+_2, 
                                         pre+"_D_K"  : _0+"( D*(2010)- -> ( [D0]cc -> ^K+ pi- pi- pi+ ) pi- )"+_2, 
                                         pre+"_D_pi1": _0+"( D*(2010)- -> ( [D0]cc -> K+ ^pi- pi- pi+ ) pi- )"+_2, 
                                         pre+"_D_pi2": _0+"( D*(2010)- -> ( [D0]cc -> K+ pi- ^pi- pi+ ) pi- )"+_2, 
                                         pre+"_D_pi3": _0+"( D*(2010)- -> ( [D0]cc -> K+ pi- pi- ^pi+ ) pi- )"+_2
                                         }
    if(d_string=="Dst0_pi0")     :  return {pre         : _0+"^( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ ) ( pi0 -> gamma gamma ) )"+_2,          
                                            pre+"_pi"   : _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ ) ^( pi0 -> gamma gamma ) )"+_2,          
                                            pre+"_pi_gamma1"   : _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ ) ( pi0 -> ^gamma gamma ) )"+_2,          
                                            pre+"_pi_gamma2"   : _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ ) ( pi0 -> gamma ^gamma ) )"+_2,          
                                            pre+"_D"    : _0+"( [D*(2007)0]cc -> ^( [D0]cc -> K- pi+ ) ( pi0 -> gamma gamma ) )"+_2,         
                                            pre+"_D_K"  : _0+"( [D*(2007)0]cc -> ( [D0]cc -> ^K- pi+ ) ( pi0 -> gamma gamma ) )"+_2,        
                                            pre+"_D_pi" : _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- ^pi+ ) ( pi0 -> gamma gamma ) )"+_2
                                            }
    if(d_string=="Dst0bar_pi0")  :  return {pre         : _0+"^( [D*(2007)~0]cc  -> ( [D0]cc -> K+ pi- ) ( pi0 -> gamma gamma ) )"+_2,          
                                            pre+"_pi"   : _0+"( [D*(2007)~0]cc  -> ( [D0]cc -> K+ pi- ) ^( pi0 -> gamma gamma ) )"+_2,          
                                            pre+"_pi_gamma1"   : _0+"( [D*(2007)~0]cc  -> ( [D0]cc -> K+ pi- ) ( pi0 -> ^gamma gamma ) )"+_2,          
                                            pre+"_pi_gamma2"   : _0+"( [D*(2007)~0]cc  -> ( [D0]cc -> K+ pi- ) ( pi0 -> gamma ^gamma ) )"+_2,          
                                            pre+"_D"    : _0+"( [D*(2007)~0]cc  -> ^( [D0]cc -> K+ pi- ) ( pi0 -> gamma gamma ) )"+_2,         
                                            pre+"_D_K"  : _0+"( [D*(2007)~0]cc  -> ( [D0]cc -> ^K+ pi- ) ( pi0 -> gamma gamma ) )"+_2,        
                                            pre+"_D_pi" : _0+"( [D*(2007)~0]cc  -> ( [D0]cc -> K+ ^pi- ) ( pi0 -> gamma gamma ) )"+_2
                                            }
    if(d_string=="Dst0bar_pi0_K3pi"):  return {pre         : _0+"^( [D*(2007)~0]cc -> ( [D0]cc -> K+ pi- pi- pi+ ) pi0 )"+_2,  
                                               pre+"_pi"   : _0+"( [D*(2007)~0]cc -> ( [D0]cc -> K+ pi- pi- pi+ ) ^pi0 )"+_2,  
                                               pre+"_D"    : _0+"( [D*(2007)~0]cc -> ^( [D0]cc -> K+ pi- pi- pi+ ) pi0 )"+_2, 
                                               pre+"_D_K"  : _0+"( [D*(2007)~0]cc -> ( [D0]cc -> ^K+ pi- pi- pi+ ) pi0 )"+_2, 
                                               pre+"_D_pi1": _0+"( [D*(2007)~0]cc -> ( [D0]cc -> K+ ^pi- pi- pi+ ) pi0 )"+_2, 
                                               pre+"_D_pi2": _0+"( [D*(2007)~0]cc -> ( [D0]cc -> K+ pi- ^pi- pi+ ) pi0 )"+_2, 
                                               pre+"_D_pi3": _0+"( [D*(2007)~0]cc -> ( [D0]cc -> K+ pi- pi- ^pi+ ) pi0 )"+_2
                                               }
    if(d_string=="Dst0_pi0_K3pi"):  return {pre         : _0+"^( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ pi+ pi- ) pi0 )"+_2,  
                                            pre+"_pi"   : _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ pi+ pi- ) ^pi0 )"+_2,  
                                            pre+"_D"    : _0+"( [D*(2007)0]cc -> ^( [D0]cc -> K- pi+ pi+ pi- ) pi0 )"+_2, 
                                            pre+"_D_K"  : _0+"( [D*(2007)0]cc -> ( [D0]cc -> ^K- pi+ pi+ pi- ) pi0 )"+_2, 
                                            pre+"_D_pi1": _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- ^pi+ pi+ pi- ) pi0 )"+_2, 
                                            pre+"_D_pi2": _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ ^pi+ pi- ) pi0 )"+_2, 
                                            pre+"_D_pi3": _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ pi+ ^pi- ) pi0 )"+_2
                                            }
    if(d_string=="Dst0_gamma")     :return {pre         : _0+"^( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ ) gamma )"+_2,          
                                            pre+"_gamma": _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ ) ^gamma )"+_2,              
                                            pre+"_D"    : _0+"( [D*(2007)0]cc -> ^( [D0]cc -> K- pi+ ) gamma )"+_2,        
                                            pre+"_D_K"  : _0+"( [D*(2007)0]cc -> ( [D0]cc -> ^K- pi+ ) gamma )"+_2,         
                                            pre+"_D_pi" : _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- ^pi+ ) gamma )"+_2
                                            }
    if(d_string=="Dst0bar_gamma")  :return {pre         : _0+"^( [D*(2007)~0]cc  -> ( [D0]cc -> K+ pi- ) gamma )"+_2,          
                                            pre+"_gamma": _0+"( [D*(2007)~0]cc  -> ( [D0]cc -> K+ pi- ) ^gamma )"+_2,          
                                            pre+"_D"    : _0+"( [D*(2007)~0]cc  -> ^( [D0]cc -> K+ pi- ) gamma )"+_2,         
                                            pre+"_D_K"  : _0+"( [D*(2007)~0]cc  -> ( [D0]cc -> ^K+ pi- ) gamma )"+_2,        
                                            pre+"_D_pi" : _0+"( [D*(2007)~0]cc  -> ( [D0]cc -> K+ ^pi- ) gamma )"+_2
                                            }
    if(d_string=="Dst0_gamma_K3pi"):return {pre         : _0+"^( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ pi+ pi- ) gamma )"+_2,  
                                            pre+"_gamma": _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ pi+ pi- ) ^gamma )"+_2,  
                                            pre+"_D"    : _0+"( [D*(2007)0]cc -> ^( [D0]cc -> K- pi+ pi+ pi- ) gamma )"+_2, 
                                            pre+"_D_K"  : _0+"( [D*(2007)0]cc -> ( [D0]cc -> ^K- pi+ pi+ pi- ) gamma )"+_2, 
                                            pre+"_D_pi1": _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- ^pi+ pi+ pi- ) gamma )"+_2, 
                                            pre+"_D_pi2": _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ ^pi+ pi- ) gamma )"+_2, 
                                            pre+"_D_pi3": _0+"( [D*(2007)0]cc -> ( [D0]cc -> K- pi+ pi+ ^pi- ) gamma )"+_2
                                            }
    if(d_string=="Dst0bar_gamma_K3pi"):return {pre         : _0+"^( [D*(2007)~0]cc -> ( [D0]cc -> K+ pi- pi- pi+ ) gamma )"+_2,  
                                               pre+"_gamma": _0+"( [D*(2007)~0]cc -> ( [D0]cc -> K+ pi- pi- pi+ ) ^gamma )"+_2,  
                                               pre+"_D"    : _0+"( [D*(2007)~0]cc -> ^( [D0]cc -> K+ pi- pi- pi+ ) gamma )"+_2, 
                                               pre+"_D_K"  : _0+"( [D*(2007)~0]cc -> ( [D0]cc -> ^K+ pi- pi- pi+ ) gamma )"+_2, 
                                               pre+"_D_pi1": _0+"( [D*(2007)~0]cc -> ( [D0]cc -> K+ ^pi- pi- pi+ ) gamma )"+_2, 
                                               pre+"_D_pi2": _0+"( [D*(2007)~0]cc -> ( [D0]cc -> K+ pi- ^pi- pi+ ) gamma )"+_2, 
                                               pre+"_D_pi3": _0+"( [D*(2007)~0]cc -> ( [D0]cc -> K+ pi- pi- ^pi+ ) gamma )"+_2
                                               }
    sys.exit();  return 'NODBRANCHFOUND' #shouldn't reach this line

  def getXbranchesDict(_0, pre, x_string):
    if(x_string=="Km"):     return {"Bach":_0+"^K- ) ]CC"}
    if(x_string=="Kp"):     return {"Bach":_0+"^K+ ) ]CC"}
    if(x_string=="pim"):     return {"Bach":_0+"^pi- ) ]CC"}
    if(x_string=="pip"):     return {"Bach":_0+"^pi+ ) ]CC"}
    import sys; sys.exit(); return 'NODBRANCHFOUND' #shouldn't reach this line
  def noCar(X):
    return X.replace('^','');
  def makeDecayDict(myB, myD1, myD2, myX, doCC, isMC=False):
    el = {"Bm":       "[ (B- -> ",
          "B0":       "[ ([B0]cc -> ",
          "B0bar":    "[ ([B~0]cc -> ",
          "D0":       " ^( [D0]cc -> ^K- ^pi+ ) ",
          "D0bar":    " ^( [D~0]cc -> ^K+ ^pi- ) ",
          "D0_K3pi":       " ^( [D0]cc -> ^K- ^pi+ ^pi+ ^pi- ) ",
          "D0bar_K3pi":    " ^( [D~0]cc -> ^K+ ^pi- ^pi- ^pi+ ) ",
          "Dp":       " ^( D+ -> ^K- ^pi+ ^pi+ ) ",
          "Dm":       " ^( D- -> ^K+ ^pi- ^pi- ) ",
          "Dsp":       " ^( D_s+ -> ^K- ^K+ ^pi+ ) ",
          "Dsm":       " ^( D_s- -> ^K+ ^K- ^pi- ) ",
          "Dstp":     " ^( D*(2010)+ -> ^( [D0]cc -> ^K- ^pi+ ) ^pi+ ) ",
          "Dstm":     " ^( D*(2010)- -> ^( [D~0]cc -> ^K+ ^pi- ) ^pi- ) ",
          "Dstp_K3pi":" ^( D*(2010)+ -> ^( [D0]cc -> ^K- ^pi+ ^pi+ ^pi- ) ^pi+ ) ",
          "Dstm_K3pi":" ^( D*(2010)- -> ^( [D~0]cc -> ^K+ ^pi- ^pi- ^pi+ ) ^pi- ) ",
          "Dst0_pi0":  " ^( [D*(2007)0]cc -> ^( [D0]cc -> ^K- ^pi+ ) ^( pi0 -> ^gamma ^gamma ) ) ",
          "Dst0_gamma":" ^( [D*(2007)0]cc -> ^( [D0]cc -> ^K- ^pi+ ) ^gamma ) ",
          "Dst0bar_pi0":  " ^( [D*(2007)~0]cc -> ^( [D~0]cc -> ^K+ ^pi- ) ^( pi0 -> ^gamma ^gamma ) ) ",
          "Dst0bar_gamma":" ^( [D*(2007)~0]cc -> ^( [D~0]cc -> ^K+ ^pi- ) ^gamma ) ",
          "pim":       " ^pi- )]CC",
          "pip":       " ^pi+ )]CC",
          "Km":       " ^K- )]CC",
          "Kp":       " ^K+ )]CC"}
    # This is to return
    branchList = ["",{}] # head, branches
    # Figure out overall 'Decay' string
    branchList[0] = el[myB]+el[myD1]+el[myD2]+el[myX]
    # Set up the branch list
    branchList[1]["B"] = el[myB]+noCar(el[myD1])+noCar(el[myD2])+noCar(el[myX])
    branchList[1].update( getDbranchesDict(noCar(el[myB]),                  "D1",myD1,  noCar(el[myD2])+noCar(el[myX])) );
    branchList[1].update( getDbranchesDict(noCar(el[myB])+noCar(el[myD1]),  "D2",myD2,  noCar(el[myX])) );
    branchList[1].update( getXbranchesDict(noCar(el[myB])+noCar(el[myD1])+noCar(el[myD2]),   "X",myX) );
    # If shouldn't do CC, remove all the CC's here
    if(doCC==False):
      branchList[0] = branchList[0].replace('CC','').replace('[','').replace(']','').replace('cc','');
      for entry in branchList[1].keys():
        branchList[1][entry] = branchList[1][entry].replace('CC','').replace('[','').replace(']','').replace('cc','');
    # Hand back the dictionary
    return branchList;

  from Configurables import DecayTreeTuple, TupleToolTrigger, TupleToolPid, TupleToolTrackInfo, TupleToolTISTOS , TupleToolStripping, TupleToolPi0Info, TupleToolPhotonInfo, TupleToolDecay
  from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool
  from Configurables import LoKi__Hybrid__EvtTupleTool as LoKiEvtTupleTool
  from Configurables import MCTupleToolDecayType, MCTupleToolHierarchy, MCTupleToolKinematic, MCTupleToolPID, MCTupleToolReconstructed, TupleToolMCBackgroundInfo

  def makeTuple(name,decayList,input,doCC=True):
    tup = DecayTreeTuple(name)
    tup.Decay = makeDecayDict(decayList[0],decayList[1],decayList[2],decayList[3],doCC)[0]
    tup.addBranches( makeDecayDict(decayList[0],decayList[1],decayList[2],decayList[3],doCC)[1] )
    tup.ToolList = ["TupleToolGeometry","TupleToolEventInfo"]
    tup.Inputs = [ input ]
    
    #=======================#
    #=== LoKi evt Variables #
    #=======================#
    lokiEvtVars={
      "Preambulo" : ['from LoKiTracks.decorators import *',
                     'from LoKiNumbers.decorators import *',
                     'from LoKiCore.functions import *'],
      "nTracks"  : "RECSUMMARY( LHCb.RecSummary.nTracks  , -1)",
      "nLong":    "RECSUMMARY( LHCb.RecSummary.nLongTracks , -1)",
      "nPVs" :    "recSummary(LHCb.RecSummary.nPVs,'Rec/Vertex/Primary')"
    }
    tool=LoKiEvtTupleTool("LoKiTool_Evt")
    preambulo = lokiEvtVars.pop("Preambulo", None);
    tool.VOID_Variables=lokiEvtVars
    tool.Preambulo=preambulo
    tup.addTool(tool)
    tup.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKiTool_Evt"]
    
    #=======================#
    #=== LoKi Variables ====#
    #=======================#
    # DTF vars
    B_LoKivars = {
               "DTF_CHI2": "DTF_CHI2( True, strings('D0','D~0','D+','D-','D_s+','D_s-' ) )",
               "DTF_NDOF": "DTF_NDOF( True, strings('D0','D~0','D+','D-','D_s+','D_s-' ) )",
               "DTF_CHI2NDOF":"DTF_CHI2NDOF( True, strings('D0','D~0','D+','D-','D_s+','D_s-' ) )",
               "DTF_CHI2_mBconstr": "DTF_CHI2( True, strings('B0','B~0','B+','B-','D0','D~0','D+','D-','D_s+','D_s-' ) )",
               "DTF_NDOF_mBconstr": "DTF_NDOF( True, strings('B0','B~0','B+','B-','D0','D~0','D+','D-','D_s+','D_s-' ) )",
               "DTF_CHI2NDOF_mBconstr":"DTF_CHI2NDOF( True, strings('B0','B~0','B+','B-','D0','D~0','D+','D-','D_s+' ) )",
               "DTF_CHI2_noMassConstr": "DTF_CHI2( True )",
               "DTF_NDOF_noMassConstr": "DTF_NDOF( True )",
               "DTF_CHI2NDOF_noMassConstr":"DTF_CHI2NDOF( True )",
               };
    for branchLab,branchString in tup.Branches.iteritems():
      for var in ['E','PX','PY','PZ','M','ID']:
        if(branchLab=="B"):
          B_LoKivars.update( {"DTF_"+branchLab+"_"+var          : "DTF_FUN ( "+var+", True, strings('D0','D~0','D+','D-','D_s+','D_s-' ) )"} );
          B_LoKivars.update( {"DTF_mBconstr_"+branchLab+"_"+var : "DTF_FUN ( "+var+", True, strings('B0','B~0','B+','B-','D0','D~0','D+','D-','D_s+','D_s-' ) )"} );
        else:
          B_LoKivars.update( {"DTF_"+branchLab+"_"+var          : "DTF_FUN ( CHILD("+var+", '"+branchString+"'), True, strings('D0','D~0','D+','D-','D_s+','D_s-' ) )"} );
          B_LoKivars.update( {"DTF_mBconstr_"+branchLab+"_"+var : "DTF_FUN ( CHILD("+var+", '"+branchString+"'), True, strings('B0','B~0','B+','B-','D0','D~0','D+','D-','D_s+','D_s-' ) )"} );
    lokiVars={"B": B_LoKivars}
    
    # Loop over branches and construct variable list
    for branch in tup.Branches.keys():
      if branch not in lokiVars.keys():  lokiVars[branch]={"Preambulo":["from LoKiTracks.decorators import *", 
                                                                       "from LoKiProtoParticles.decorators import *"]}
      if branch=="B":
        lokiVars[ branch ]["BPVDIRA"]="BPVDIRA"
        lokiVars[ branch ]["BPVIPCHI2"]="BPVIPCHI2()"
        lokiVars[ branch ]["ptasy_1.50"] = "INFO(9000+11,0.)"
      
      # For all particles
      lokiVars[ branch ]["M"]="M";
      lokiVars[ branch ]["PX"]="PX";
      lokiVars[ branch ]["PY"]="PY";
      lokiVars[ branch ]["PZ"]="PZ";
      lokiVars[ branch ]["E"]="E";
      lokiVars[ branch ]["Q"]="Q";
      lokiVars[ branch ]["IPCHI2_BPV"]="BPVIPCHI2()";
      lokiVars[ branch ]["P"]="P";
      lokiVars[ branch ]["PT"]="PT";
      if branch in ["D1_gamma"]:
        lokiVars[ branch ]["CL"]="CL"
      # For all non-stable particles, including the Bachelor if it is KS, phi or K*
      if branch in ["B","D1","D2","D1_D","D2_D","Bach"]:
        if(branch != "Bach"):
          lokiVars[ branch ]["ENDVTX_CHI2"]="VFASPF(VCHI2)";
          lokiVars[ branch ]["FDCHI2_BPV"] ="BPVVDCHI2";
        elif (tup.Decay.find("KS0")!=-1 or tup.Decay.find("K*(892)")!=-1 or tup.Decay.find("phi(1020)")!=-1):
          lokiVars[ branch ]["ENDVTX_CHI2"]="VFASPF(VCHI2)";
      
      # For all D daughters
      if branch in ["D1_K","D1_pi","D1_K1","D1_K2","D1_pi1","D1_pi2","D1_pi3",
                    "D2_K","D2_pi","D2_K1","D2_K2","D2_pi1","D2_pi2","D2_pi3",
                    "D1_D_K","D1_D_pi","D1_D_K1","D1_D_K2","D1_D_pi1","D1_D_pi2","D1_D_pi3",
                    "D2_D_K","D2_D_pi","D2_D_K1","D2_D_K2","D2_D_pi1","D2_D_pi2","D2_D_pi3","Bach"]:
        lokiVars[ branch ]["PIDK"]="PIDK"
        lokiVars[ branch ]["PIDp"]="PIDp"
        lokiVars[ branch ]["ProbNNK"]="PROBNNk"
        lokiVars[ branch ]["ProbNNpi"]="PROBNNpi"
        lokiVars[ branch ]["ProbNNp"]="PROBNNp"
        lokiVars[ branch ]["ProbNNmu"]="PROBNNmu"
        lokiVars[ branch ]["TRCHI2DOF"]="TRCHI2DOF"
        lokiVars[ branch ]["Tr_GhostProb"] = "TRGHP"
        lokiVars[ branch ]["hasRich"] = "switch( PPCUT(PP_HASRICHPID),1 , 0)"
        lokiVars[ branch ]["RiThresholdPi"]   = "switch( PPCUT(PP_RICHTHRES_PI),1 , 0)"
        lokiVars[ branch ]["RiThresholdKa"]   = "switch( PPCUT(PP_RICHTHRES_K),1 , 0)"
        if branch != 'Bach':
          lokiVars[ branch ]["isMu"]            = "switch( ISMUON,1 , 0)"

    conf_toolLists={}
    # Loop over the Tuples branches, and if find them in the LoKi variables dictionary, add the appropriate variables
    for branch in tup.Branches.keys():
      dec = TupleToolDecay(branch)
      if branch in lokiVars:
        tool = LoKiTupleTool("LoKiTool_%s" %branch);
        preambulo = lokiVars[branch].pop("Preambulo", None);
        tool.Variables = lokiVars[branch]
        if preambulo is not None:
          tool.Preambulo += preambulo
        dec.addTool(tool)
        dec.ToolList=["LoKi::Hybrid::TupleTool/LoKiTool_%s" %branch]
      if branch in conf_toolLists:
        dec.ToolList+=conf_toolLists(branch)
      tup.addTool(dec)

    # Add TIS TOS info to the B
    tup.B.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
    tup.B.TupleToolTISTOS.VerboseL0=True
    tup.B.TupleToolTISTOS.VerboseHlt1=True
    tup.B.TupleToolTISTOS.VerboseHlt2=True
    tup.B.TupleToolTISTOS.TriggerList= mytriggers
    tup.addTupleTool(TupleToolStripping, name="TupleToolStripping");
    tup.TupleToolStripping.StrippingList = mystrips

    if "D1_pi_gamma1" in tup.Branches.keys():
      tup.D1_pi.addTupleTool(TupleToolPi0Info, name="TupleToolPi0Info");
      tup.D1_pi_gamma1.addTupleTool(TupleToolPhotonInfo, name="TupleToolPhotonInfo");
      tup.D1_pi_gamma2.addTupleTool(TupleToolPhotonInfo, name="TupleToolPhotonInfo");
    if "D1_gamma" in tup.Branches.keys():
      tup.D1_gamma.addTupleTool(TupleToolPhotonInfo, name="TupleToolPhotonInfo");

    return tup;

  ########################################
  # Make the tuples
  ########################################
  tupleList=[]

  # B+ modes
  #tupleList+=[ makeTuple("BmDstpDmKm",      ["Bm","Dstp","Dm","Km"],        seq["BmDstpDmKm"].outputLocation() ) ] 
  #tupleList+=[ makeTuple("BmDstmDpKm",      ["Bm","Dstm","Dp","Km"],        seq["BmDstpDmKm"].outputLocation() ) ] 
  #tupleList+=[ makeTuple("BmDstp_K3piDmKm", ["Bm","Dstp_K3pi","Dm","Km"],   seq["BmDstp_K3piDmKm"].outputLocation() ) ] 
  #tupleList+=[ makeTuple("BmDstm_K3piDpKm", ["Bm","Dstm_K3pi","Dp","Km"],   seq["BmDstp_K3piDmKm"].outputLocation() ) ] 
  
  # B0 modes
  #tupleList+=[ makeTuple("B0DstmD0Kp",      ["B0","Dstm","D0","Kp"],        seq["B0DstmD0Kp"].outputLocation() ) ]   
  #tupleList+=[ makeTuple("B0Dstm_K3piD0Kp", ["B0","Dstm_K3pi","D0","Kp"],   seq["B0Dstm_K3piD0Kp"].outputLocation() ) ]  
  #tupleList+=[ makeTuple("B0DstmD0_K3piKp", ["B0","Dstm","D0_K3pi","Kp"],   seq["B0DstmD0_K3piKp"].outputLocation() ) ]  
  
  # reference channels
  #tupleList+=[ makeTuple("BmD0D0barKm",           ["Bm","D0","D0bar","Km"],             seq["BmD0D0barKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmD0_K3piD0bar_K3piKm", ["Bm","D0_K3pi","D0bar_K3pi","Km"],   seq["BmD0_K3piD0bar_K3piKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmD0_K3piD0barKm",      ["Bm","D0_K3pi","D0bar","Km"],        seq["BmD0_K3piD0barKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmD0D0bar_K3piKm",      ["Bm","D0","D0bar_K3pi","Km"],        seq["BmD0D0bar_K3piKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0DmD0Kp",              ["B0","Dm","D0","Kp"],                seq["B0DmD0Kp"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0DmD0_K3piKp",         ["B0","Dm","D0_K3pi","Kp"],           seq["B0DmD0_K3piKp"].outputLocation() ) ]

  # Dstar and Ds
  tupleList+=[ makeTuple("BmDstpDsmKm",             ["Bm","Dstp","Dsm","Km"],               seq["BmDstpDsmKm"].outputLocation() ) ]
  tupleList+=[ makeTuple("BmDstmDspKm",             ["Bm","Dstm","Dsp","Km"],               seq["BmDstmDspKm"].outputLocation() ) ]
  tupleList+=[ makeTuple("BmDstpDsmpim",            ["Bm","Dstp","Dsm","pim"],              seq["BmDstpDsmpim"].outputLocation() ) ]
  tupleList+=[ makeTuple("BmDstmDsppim",            ["Bm","Dstm","Dsp","pim"],              seq["BmDstmDsppim"].outputLocation() ) ]
  tupleList+=[ makeTuple("BmDstm_K3piDspKm",        ["Bm","Dstm_K3pi","Dsp","Km"],          seq["BmDstm_K3piDspKm"].outputLocation() ) ]
  tupleList+=[ makeTuple("BmDstp_K3piDsmKm",        ["Bm","Dstp_K3pi","Dsm","Km"],          seq["BmDstp_K3piDsmKm"].outputLocation() ) ]
  tupleList+=[ makeTuple("BmDstm_K3piDsppim",       ["Bm","Dstm_K3pi","Dsp","pim"],         seq["BmDstm_K3piDsppim"].outputLocation() ) ]
  tupleList+=[ makeTuple("BmDstp_K3piDsmpim",       ["Bm","Dstp_K3pi","Dsm","pim"],         seq["BmDstp_K3piDsmpim"].outputLocation() ) ]

  from Configurables import EventTuple
  etuple=EventTuple()
  etuple.ToolList = ["TupleToolEventInfo"]#, "TupleToolPrimaries"]
  et_ttt=etuple.addTupleTool("TupleToolTrigger");
  et_ttt.VerboseL0=True
  et_ttt.VerboseHlt1=True
  et_ttt.VerboseHlt2=True
  et_ttt.TriggerList=mytriggers

  ########################################
  # Track scale (data) 
  ########################################

  from Configurables import TrackScaleState as SCALER
  scaler=SCALER('Scaler')

  seqToReturn = [ scaler ] + seqList + tupleList + [ etuple ]

  return seqToReturn
