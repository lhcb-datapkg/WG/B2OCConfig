"""
Stripping filtering file for B^\pm -> D0(*) h^\pm MC
@date   2017-04-20

Margarete Schellenberg, 13/12/2017
copy of Filter_B2OC_bdh_strip28.py 
Stripping version S28 -> S28r1 
StrippingTCK 0x41432800 --> 0x41442810

Lei Hao, 21/3/2019
copy of  Filter_B2OC_bdh_strip28.py
Stripping version s28r1 -> s29r2
StrippingTCK 0x41442810 --> 0x42722920

Lei Hao, 21/3/2019
copy of Filter_B2OC_bdh_strip29r2.py
Stripping version s29r2 -> s34
StrippingTCK 0x42722920 --> 0x44403400

Seophine Stanislaus, 30/10/2021
adapted Filter_B2OC_bdh_strip34.py
DST output -> MDST output

"""

year = 2018
assert(year in [2015, 2016, 2017, 2018])

#stripping version
#stripping='stripping28r1'
stripping = {2015: 'stripping24', 2016: 'stripping28r1', 2017: 'stripping29r2p1', 2018: 'stripping34'}[year]

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )
#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)
# Select my lines
AllStreams = StrippingStream("bdsth.Strip")
linesToAdd = []
MyLines = [ 'StrippingB2D0PiD2KSHHDDBeauty2CharmLine',
            'StrippingB2D0PiD2KSHHDDWSBeauty2CharmLine',
            'StrippingB2D0PiD2KSHHLLBeauty2CharmLine',
            'StrippingB2D0PiD2KSHHLLWSBeauty2CharmLine',
            'StrippingB2D0KD2KSHHDDBeauty2CharmLine',
            'StrippingB2D0KD2KSHHDDWSBeauty2CharmLine',
            'StrippingB2D0KD2KSHHLLBeauty2CharmLine',
            'StrippingB2D0KD2KSHHLLWSBeauty2CharmLine',
            'StrippingB2Dstar0KDst02D0GammaD2KSHHDDBeauty2CharmLine',
            'StrippingB2Dstar0KDst02D0GammaD2KSHHDDWSBeauty2CharmLine',
            'StrippingB2Dstar0KDst02D0GammaD2KSHHLLBeauty2CharmLine',
            'StrippingB2Dstar0KDst02D0GammaD2KSHHLLWSBeautyCharmLine',
            'StrippingB2Dstar0KDst02D0Pi0D2KSHHDDBeauty2CharmLine',
            'StrippingB2Dstar0KDst02D0Pi0D2KSHHDDWSBeauty2CharmLine',
            'StrippingB2Dstar0KDst02D0Pi0D2KSHHLLBeauty2CharmLine',
            'StrippingB2Dstar0KDst02D0Pi0D2KSHHLLWSBeauty2CharmLine',
            'StrippingB2Dstar0PiDst02D0GammaD2KSHHDDBeauty2CharmLine',
            'StrippingB2Dstar0PiDst02D0GammaD2KSHHDDWSBeauty2CharmLine',
            'StrippingB2Dstar0PiDst02D0GammaD2KSHHLLBeauty2CharmLine',
            'StrippingB2Dstar0PiDst02D0GammaD2KSHHLLWSBeautyCharmLine',
            'StrippingB2Dstar0PiDst02D0Pi0D2KSHHDDBeauty2CharmLine',
            'StrippingB2Dstar0PiDst02D0Pi0D2KSHHDDWSBeauty2CharmLine',
            'StrippingB2Dstar0PiDst02D0Pi0D2KSHHLLBeauty2CharmLine',
            'StrippingB2Dstar0PiDst02D0Pi0D2KSHHLLWSBeauty2CharmLine'
          ]

for stream in streams:
    if 'Bhadron' in stream.name():
        for line in stream.lines:
            line._prescale = 1.0
            if line.name() in MyLines:
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)



sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )


AllStreams.sequence().IgnoreFilterPassed = False # so that we do not get all events written out

#

# Configuration of SelDSTWriter
#
###For DST
'''
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }
'''
#STANDARD CONFIGURATION FOR MDST OUTPUT
##################################################################
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf 
                                      )

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
    }
##################################################################


#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK = {2015: 0x44502400, 2016: 0x44502810, 2017: 0x44502920, 2018: 0x44503400}[year])

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                     # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().DataType = "2018"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
