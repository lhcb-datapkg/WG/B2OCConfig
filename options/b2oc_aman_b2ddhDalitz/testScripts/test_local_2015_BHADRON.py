from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
     'PFN:root://x509up_u32781@marsedpm.in2p3.fr//dpm/in2p3.fr/home/lhcb/LHCb/Collision15/BHADRON.MDST/00102356/0000/00102356_00000729_1.bhadron.mdst'] )

import sys
sys.path.insert(0,"/afs/cern.ch/user/j/johndan/Analysis/B2DDh/lhcb-b2oc-ddk-dalitz/DataProcessing/Data/2_WGProd/DaVinciDev_v42r6p1/WG/B2OCConfig/python")

# Helper file to define data type for davinci

the_year      = '2015'
the_fileType  = 'MDST'
the_rootintes = "/Event/Bhadron"

from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
               RootInTES                 = the_rootintes      ,
             )
from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from b2oc_aman_b2ddhDalitz.selectB2DDh import configure_b2oc_aman_ddhDalitz_selection
dv.appendToMainSequence( configure_b2oc_aman_ddhDalitz_selection( dataYear = the_year ) )

db = CondDB  ( LatestGlobalTagByDataType = the_year )

if '__main__' == __name__ :

  print ' The year : ', the_year

# ================================
# Read only fired events for speed
# ================================

from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS('StrippingB02D0DKBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0DKD02K3PiBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2D0D0KBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2D0D0KD02HHD02HHBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2D0D0KD02K3PiD02K3PiBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB2D0D0KD02HHD02K3PiBeauty2CharmLineDecision') """
    )

# ================================
# Configure DaVinci
# ================================

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci

davinci = DaVinci (
    EventPreFilters = fltrs.filters ('Filters') ,
    EvtMax          =     -1    ,
    PrintFreq       =   1000    ,
    Lumi            =   True    ,
    TupleFile       = "B2OC_AMAN_B2DDHDALITZSEL_2015.ROOT"
    )

