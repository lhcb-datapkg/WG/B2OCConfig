"""
Stripping filtering file for B0 -> D0 Kst0, D0 -> Kshh MC
@author Hannah Pullen
@date   2019-06-03
"""

year = 18
strip_name = "B02DKst.Strip"

strippings = {
        16: "stripping28r1",
        17: "stripping29r2",
        18: "stripping34",
        }
TCKs = {
        16: 0x41442810, #DaVinci/v41r4p4, Stripping28r1
        17: 0x42722920, #DaVinci/v42r7p2, Stripping29r2
        18: 0x44403400, #DaVinci/v44r4  , Stripping34
        }

stripping = strippings[year]
useTCK = TCKs[year]

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )
#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)
# Select my lines
AllStreams = StrippingStream(strip_name)
linesToAdd = []
MyLines = ['StrippingB02D0KKD2KSHHDDBeauty2CharmLine',
           'StrippingB02D0KKD2KSHHDDWSBeauty2CharmLine',
           'StrippingB02D0KKD2KSHHLLBeauty2CharmLine',
           'StrippingB02D0KKD2KSHHLLWSBeauty2CharmLine',
           'StrippingB02D0KPiD2KSHHDDBeauty2CharmLine',
           'StrippingB02D0KPiD2KSHHDDWSBeauty2CharmLine',
           'StrippingB02D0KPiD2KSHHLLBeauty2CharmLine',
           'StrippingB02D0KPiD2KSHHLLWSBeauty2CharmLine',
           'StrippingB02D0MuMuD2KSHHDDBeauty2CharmLine',
           'StrippingB02D0MuMuD2KSHHLLBeauty2CharmLine',
           'StrippingB02D0MuMuWSD2KSHHDDBeauty2CharmLine',
           'StrippingB02D0MuMuWSD2KSHHLLBeauty2CharmLine',
           'StrippingB02D0PiPiD2KSHHDDBeauty2CharmLine',
           'StrippingB02D0PiPiD2KSHHDDWSBeauty2CharmLine',
           'StrippingB02D0PiPiD2KSHHLLBeauty2CharmLine',
           'StrippingB02D0PiPiD2KSHHLLWSBeauty2CharmLine',
           'StrippingB02DHHWSD2KSHHDDBeauty2CharmLine',
           'StrippingB02DHHWSD2KSHHDDWSBeauty2CharmLine',
           'StrippingB02DHHWSD2KSHHLLBeauty2CharmLine',
           'StrippingB02DHHWSD2KSHHLLWSBeauty2CharmLine',
           'StrippingB02DstarKDst2D0Pi_D2KSHHDDBeauty2CharmLine',
           'StrippingB02DstarKDst2D0Pi_D2KSHHLLBeauty2CharmLine',
           'StrippingB02DstarMuNuDst2D0Pi_D2KSHHDDBeauty2CharmLine',
           'StrippingB02DstarMuNuDst2D0Pi_D2KSHHLLBeauty2CharmLine',
           'StrippingB02DstarMuNuWSDst2D0Pi_D2KSHHDDBeauty2CharmLine',
           'StrippingB02DstarMuNuWSDst2D0Pi_D2KSHHLLBeauty2CharmLine',
           'StrippingB02DstarPiDst2D0Pi_D2KSHHDDBeauty2CharmLine',
           'StrippingB02DstarPiDst2D0Pi_D2KSHHLLBeauty2CharmLine',
           'StrippingB2D0KD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0KD2KSHHDDWSBeauty2CharmLine',
           'StrippingB2D0KD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0KD2KSHHLLWSBeauty2CharmLine',
           'StrippingB2D0KKPiD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0KKPiD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0KPi0MergedD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0KPi0MergedD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0KPi0ResolvedD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0KPi0ResolvedD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0KPiPiD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0KPiPiD2KSHHDDWSBeauty2CharmLine',
           'StrippingB2D0KPiPiD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0KPiPiD2KSHHLLWSBeauty2CharmLine',
           'StrippingB2D0KsPiDDD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0KsPiDDD2KSHHDDWSBeauty2CharmLine',
           'StrippingB2D0KsPiDDD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0KsPiDDD2KSHHLLWSBeauty2CharmLine',
           'StrippingB2D0KsPiLLD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0KsPiLLD2KSHHDDWSBeauty2CharmLine',
           'StrippingB2D0KsPiLLD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0KsPiLLD2KSHHLLWSBeauty2CharmLine',
           'StrippingB2D0MuNuD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0MuNuD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0PiD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0PiD2KSHHDDWSBeauty2CharmLine',
           'StrippingB2D0PiD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0PiD2KSHHLLWSBeauty2CharmLine',
           'StrippingB2D0PiPi0MergedD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0PiPi0MergedD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0PiPi0ResolvedD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0PiPi0ResolvedD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0PiPiPiD2KSHHDDBeauty2CharmLine',
           'StrippingB2D0PiPiPiD2KSHHDDWSBeauty2CharmLine',
           'StrippingB2D0PiPiPiD2KSHHLLBeauty2CharmLine',
           'StrippingB2D0PiPiPiD2KSHHLLWSBeauty2CharmLine',
           'StrippingB2Dstar0KDst02D0GammaD2KSHHDDBeauty2CharmLine',
           'StrippingB2Dstar0KDst02D0GammaD2KSHHDDWSBeauty2CharmLine',
           'StrippingB2Dstar0KDst02D0GammaD2KSHHLLBeauty2CharmLine',
           'StrippingB2Dstar0KDst02D0GammaD2KSHHLLWSBeauty2CharmLine',
           'StrippingB2Dstar0KDst02D0Pi0D2KSHHDDBeauty2CharmLine',
           'StrippingB2Dstar0KDst02D0Pi0D2KSHHDDWSBeauty2CharmLine',
           'StrippingB2Dstar0KDst02D0Pi0D2KSHHLLBeauty2CharmLine',
           'StrippingB2Dstar0KDst02D0Pi0D2KSHHLLWSBeauty2CharmLine',
           'StrippingB2Dstar0PiDst02D0GammaD2KSHHDDBeauty2CharmLine',
           'StrippingB2Dstar0PiDst02D0GammaD2KSHHDDWSBeauty2CharmLine',
           'StrippingB2Dstar0PiDst02D0GammaD2KSHHLLBeauty2CharmLine',
           'StrippingB2Dstar0PiDst02D0GammaD2KSHHLLWSBeauty2CharmLine',
           'StrippingB2Dstar0PiDst02D0Pi0D2KSHHDDBeauty2CharmLine',
           'StrippingB2Dstar0PiDst02D0Pi0D2KSHHDDWSBeauty2CharmLine',
           'StrippingB2Dstar0PiDst02D0Pi0D2KSHHLLBeauty2CharmLine',
           'StrippingB2Dstar0PiDst02D0Pi0D2KSHHLLWSBeauty2CharmLine']

for stream in streams:
    if 'Bhadron' in stream.name():
        for line in stream.lines:
            line._prescale = 1.0
            if line.name() in MyLines:
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)



sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )


AllStreams.sequence().IgnoreFilterPassed = False # so that we do not get all events written out

#

# Configuration of SelDSTWriter
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf 
                                      )

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
    }



dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=useTCK)# TCK = 0xVVVVSSSS, where VVVV is the DaVinci version, and SSSS the Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                     # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60


