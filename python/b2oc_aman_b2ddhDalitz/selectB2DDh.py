def configure_b2oc_aman_ddhDalitz_selection( locationRoot = "" , dataYear = "" , isSimulation = False ) :
  from PhysSelPython.Wrappers import AutomaticData, SelectionSequence, Selection, MergedSelection
  #from Gaudi.Configuration import *
  import sys

  # ================================
  # Prep trigger and stripping list
  # ================================

  mytriggers = [  "L0HadronDecision","L0HadronNoSPDDecision","L0MUON,minbiasDecision","L0MuonDecision","L0MuonNoSPDDecision",
                  "L0PhotonDecision","L0ElectronDecision","L0ElectronHiDecision","L0DiMuonDecision",
                  "Hlt1TrackAllL0Decision","Hlt1TrackMVADecision","Hlt1TwoTrackMVADecision",
                  "Hlt2Topo2BodyBBDTDecision","Hlt2Topo3BodyBBDTDecision","Hlt2Topo4BodyBBDTDecision","Hlt2IncPhiDecision",
                  "Hlt2Topo2BodyDecision","Hlt2Topo3BodyDecision","Hlt2Topo4BodyDecision","Hlt2PhiIncPhiDecision",
               ]
               
  mystrips   = [  "StrippingB02D0DKBeauty2CharmLineDecision",
                  "StrippingB02D0DPiBeauty2CharmLineDecision",
                  "StrippingB2D0D0KBeauty2CharmLineDecision", "StrippingB2D0D0KD02HHD02HHBeauty2CharmLineDecision", 
                  "StrippingB2D0D0KD02K3PiD02K3PiBeauty2CharmLineDecision","StrippingB2D0D0KD02HHD02K3PiBeauty2CharmLineDecision",
                  "StrippingB2DDKBeauty2CharmLineDecision" ,
                  "StrippingB2D0DKSLLBeauty2CharmLineDecision" , "StrippingB2D0DKSDDBeauty2CharmLineDecision",
                  "StrippingB02D0D0KSD02HHD02HHLLBeauty2CharmLineDecision", "StrippingB02D0D0KSD02HHD02HHDDBeauty2CharmLineDecision",
                  "StrippingB2DDPiBeauty2CharmLineDecision",
                  "StrippingB02DDKSDDBeauty2CharmLineDecision","StrippingB02DDKSLLBeauty2CharmLineDecision",
                  "StrippingB02D0DKD02K3PiBeauty2CharmLineDecision","StrippingB02D0DPiD02K3PiBeauty2CharmLineDecision",
                  "StrippingB02D0D0KstD02HHD02HHBeauty2CharmLineDecision","StrippingB02D0D0KstD02HHD02K3PiBeauty2CharmLineDecision",
                  "StrippingB02D0D0KstD02K3PiD02K3PiBeauty2CharmLineDecision"
               ]

  #===================================#
  #=== Prepare PID substituters ======#
  #===================================#
  from Configurables import SubstitutePID
   # Solo (historically "bachelor") kaon
  def doBachPIDswap(source,target):
    subBachKtoPi = SubstitutePID("subBachKtoPi",
                               Code="DECTREE('Beauty -> Xc Xc Xs')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> Xc Xc ^K-':'pi-',
                                                 'Beauty -> Xc Xc ^K+':'pi+'}
                               );
    newSel = Selection("sel"+target,Algorithm=subBachKtoPi,RequiredSelections=[sel[source]]);
    return newSel;
    # anyB -> D+ D- X change both D -> D_s
  def doPIDswap_B2DpDmX_to_B2DspDsmX(source,target):
    sub_step1_B2DpDmX_to_B2DspDsmX = SubstitutePID("sub_step1_B2DpDmX_to_B2DspDsmX",
                               Code="DECTREE('Beauty -> D+ D- Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> ^D+ Xc Meson':'D_s+'}
                               );
    newSel_step1 = Selection("sel_step1_"+target,Algorithm=sub_step1_B2DpDmX_to_B2DspDsmX,RequiredSelections=[sel[source]]);
    sub_step2_B2DpDmX_to_B2DspDsmX = SubstitutePID("sub_step2_B2DpDmX_to_B2DspDsmX",
                               Code="DECTREE('Beauty -> D_s+ D- Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> D_s+ ^D- Meson':'D_s-'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_step2_B2DpDmX_to_B2DspDsmX,RequiredSelections=[newSel_step1]);
    return newSel;
  
    # B- -> D+ (=> Ds+) X (with solo pi/K)
  def doPIDswap_Bm2DpX_to_Bm2DspX(source,target):
    sub_Bm2DpX_to_Bm2DspX = SubstitutePID("sub_Bm2DpX_to_Bm2DspX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> ^D+ Xc Meson':'D_s+',
                                                 'B+ -> ^D- Xc Meson':'D_s-'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_Bm2DpX_to_Bm2DspX,RequiredSelections=[sel[source]]);
    return newSel;
    # B- -> D+ (=> Ds+) D (with solo pi/K)
  def doPIDswap_Bm2DpDmX_to_Bm2DspDmX(source,target):
    sub_Bm2DpDmX_to_Bm2DspDmX = SubstitutePID("sub_Bm2DpDmX_to_Bm2DspDmX",
                               Code="DECTREE('Beauty -> D+ D- Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> ^D+ Xc Meson':'D_s+',
                                                 'B+ -> ^D- Xc Meson':'D_s-'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_Bm2DpDmX_to_Bm2DspDmX,RequiredSelections=[sel[source]]);
    return newSel;
    # B- -> D+ D (=>Ds-) (with solo pi/K)
  def doPIDswap_Bm2DpDmX_to_Bm2DpDsmX(source,target):
    sub_Bm2DpDmX_to_Bm2DpDsmX = SubstitutePID("sub_Bm2DpDmX_to_Bm2DpDsmX",
                               Code="DECTREE('Beauty -> D+ D- Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> Xc ^D- Meson':'D_s-',
                                                 'B+ -> Xc ^D+ Meson':'D_s+'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_Bm2DpDmX_to_Bm2DpDsmX,RequiredSelections=[sel[source]]);
    return newSel;
    # B- -> D+ D- X change both D -> D_s
  def doPIDswap_Bm2DpDmX_to_Bm2DspDsmX(source,target):
    sub_step1_Bm2DpDmX_to_Bm2DspDsmX = SubstitutePID("sub_step1_Bm2DpDmX_to_Bm2DspDsmX",
                               Code="DECTREE('Beauty -> D+ D- Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> ^D+ Xc Meson':'D_s+',
                                                'B+ -> ^D- Xc Meson':'D_s-'}
                               );
    newSel_step1 = Selection("sel_step1_"+target,Algorithm=sub_step1_Bm2DpDmX_to_Bm2DspDsmX,RequiredSelections=[sel[source]]);
    sub_step2_Bm2DpDmX_to_Bm2DspDsmX = SubstitutePID("sub_step2_Bm2DpDmX_to_Bm2DspDsmX",
                               Code="DECTREE('Beauty -> D_s+ D- Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'B- -> D_s+ ^D- Meson':'D_s-',
                                                'B+ -> D_s- ^D- Meson':'D_s+'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_step2_Bm2DpDmX_to_Bm2DspDsmX,RequiredSelections=[newSel_step1]);
    return newSel;
  
    # B0 -> D- (=> Ds-) D0 X+ (just look for the D which isn't a D0)
  def doPIDswap_B02DmD0X_to_B02DsmD0X(source,target):
    sub_B02DmD0X_to_B02DsmD0X = SubstitutePID("sub_B02DmD0X_to_B02DsmD0X",
                               Code="DECTREE('Beauty -> Xc D0 Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> ^D- D0 Meson':'D_s-',
                                                 'Beauty -> ^D+ D0 Meson':'D_s+'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_B02DmD0X_to_B02DsmD0X,RequiredSelections=[sel[source]]);
    return newSel;
  
    # B0 -> D+ (=> Ds-) D0 X- (just look for the D which isn't a D0)
  def doPIDswap_B02DpD0X_to_B02DspD0X(source,target):
    sub_B02DpD0X_to_B02DspD0X = SubstitutePID("sub_B02DpD0X_to_B02DspD0X",
                               Code="DECTREE('Beauty -> Xc D0 Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> ^D+ D0 Meson':'D_s+',
                                                 'Beauty -> ^D- D0 Meson':'D_s-'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_B02DpD0X_to_B02DspD0X,RequiredSelections=[sel[source]]);
    return newSel;
    # B0 -> D0bar D- (=> Ds-) X+ (just look for the D which isn't a D0)
  def doPIDswap_B02D0barDmX_to_B02D0barDsmX(source,target):
    sub_B02D0barDmX_to_B02D0barDsmX = SubstitutePID("sub_B02D0barDmX_to_B02D0barDsmX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> Xc ^D- Meson':'D_s-',
                                                 'Beauty -> Xc ^D+ Meson':'D_s+'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_B02D0barDmX_to_B02D0barDsmX,RequiredSelections=[sel[source]]);
    return newSel;
    # B0 -> D- (=> Ds-) D0bar X+ (just look for the D which isn't a D0)
  def doPIDswap_B02DmD0barX_to_B02DsmD0barX(source,target):
    sub_B02DmD0barX_to_B02DsmD0barX = SubstitutePID("sub_B02DmD0barX_to_B02DsmD0barX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> ^D- Xc Meson':'D_s-',
                                                'Beauty -> ^D+ Xc Meson':'D_s+'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_B02DmD0barX_to_B02DsmD0barX,RequiredSelections=[sel[source]]);
    return newSel;
    # B0 -> D+ (=> Ds+) D0bar X- (just look for the D which isn't a D0)
  def doPIDswap_B02DpD0barX_to_B02DspD0barX(source,target):
    sub_B02DpD0barX_to_B02DspD0barX = SubstitutePID("sub_B02DpD0barX_to_B02DspD0barX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> ^D+ Xc Meson':'D_s+',
                                                'Beauty -> ^D- Xc Meson':'D_s-'}
                               );
    newSel = Selection("sel"+target,Algorithm=sub_B02DpD0barX_to_B02DspD0barX,RequiredSelections=[sel[source]]);
    return newSel;

  # B0 -> D+ (=> Ds+) D(*)- KS (just change one D D+ or D-)
  def doPIDswap_B02DDX_to_B02DsDX(source,target):
    sub_B02DpDmX_to_B02DspDmX = SubstitutePID("sub_B02DpDmX_to_B02DspDmX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> ^D+ Xc Meson':'D_s+'}
                               );
    newSel1 = Selection("sel1"+target,Algorithm=sub_B02DpDmX_to_B02DspDmX,RequiredSelections=[sel[source]]);
    sub_B02DpDmX_to_B02DpDsmX = SubstitutePID("sub_B02DpDmX_to_B02DsDsmX",
                               Code="DECTREE('Beauty -> Xc Xc Meson')",
                               MaxChi2PerDoF = -1,
                               Substitutions = {'Beauty -> Xc ^D- Meson':'D_s-'}
                               );
    newSel2 = Selection("sel2"+target,Algorithm=sub_B02DpDmX_to_B02DpDsmX,RequiredSelections=[sel[source]]);
    return MergedSelection("sel"+target, RequiredSelections=[newSel1,newSel2]);
  # B0 -> D0 D0bar Kst (=> phi)
  def doPIDswap_B02D0D0Kst_to_B02D0D0Phi(source,target):
    sub_step1_B02D0D0Kst_to_B02D0D0Phi = SubstitutePID("sub_step1_B02D0D0Kst_to_B02D0D0Phi",
                                  Code="DECTREE('Beauty -> Xc Xc K*(892)0')",
                                  MaxChi2PerDoF = -1,
                                  Substitutions = {'Beauty -> Xc Xc ( K*(892)0 -> K+ ^pi- )':'K-'} 
                              );
    newSel = Selection("sel"+target,Algorithm=sub_step2_B02D0D0Kst_to_B02D0D0Phi,RequiredSelections=[newSel_step1])
    return newSel;

  ########################################
  # Get candidates from their stripping
  # location
  ########################################


  # Selections
  sel={}
  
  # BmD0D0barKm 
  sel["BmD0D0barKm"]               = AutomaticData(Location = locationRoot+"Phys/B2D0D0KD02HHD02HHBeauty2CharmLine/Particles")
  sel["BmD0_K3piD0bar_K3piKm"]     = AutomaticData(Location = locationRoot+"Phys/B2D0D0KD02K3PiD02K3PiBeauty2CharmLine/Particles")
  sel["BmD0_K3piD0barKm"]          = AutomaticData(Location = locationRoot+"Phys/B2D0D0KD02HHD02K3PiBeauty2CharmLine/Particles")
  sel["BmD0D0bar_K3piKm"]          = AutomaticData(Location = locationRoot+"Phys/B2D0D0KD02HHD02K3PiBeauty2CharmLine/Particles")

  # BmDpDmKm
  sel["BmDpDmKm_preFilter"]        = AutomaticData(Location = locationRoot+"Phys/B2DDKBeauty2CharmLine/Particles")
  # Filter these D+D-h lines
  from Configurables import FilterDesktop
  filter_DpDmK = FilterDesktop('filter_DpDmK', Code = "BPVIPCHI2()<30")
  #"(M>5000*MeV) & (M<5800*MeV) & \
  #                              INTREE( ('D+'==ABSID) & (M>1820*MeV) & (M<1910*MeV) )")
  sel["BmDpDmKm"] = Selection(name="selBmDpDmKm", Algorithm=filter_DpDmK, RequiredSelections=[sel["BmDpDmKm_preFilter"] ] )
 
  # B0D0barDmKp
  sel["B0D0DmKp"]                  = AutomaticData(Location = locationRoot+"Phys/B02D0DKBeauty2CharmLine/Particles")
  sel["B0D0barDpKm"]=sel["B0D0DmKp"] # For MC naming
  sel["B0D0_K3piDmKp"]             = AutomaticData(Location = locationRoot+"Phys/B02D0DKD02K3PiBeauty2CharmLine/Particles")
 
  # BmD0DmKS
  sel["BmD0DmKSLL"]                = AutomaticData(Location = locationRoot+"Phys/B2D0DKSLLBeauty2CharmLine/Particles")
  sel["BmD0DmKSDD"]                = AutomaticData(Location = locationRoot+"Phys/B2D0DKSDDBeauty2CharmLine/Particles")
 
  # B0D0D0KS
  sel["B0D0D0barKSLL"]             = AutomaticData(Location = locationRoot+"Phys/B02D0D0KSD02HHD02HHLLBeauty2CharmLine/Particles")
  sel["B0D0D0barKSDD"]             = AutomaticData(Location = locationRoot+"Phys/B02D0D0KSD02HHD02HHDDBeauty2CharmLine/Particles")

  # BmDpDmPim
  sel["BmDpDmpim"]                 = AutomaticData(Location = locationRoot+"Phys/B2DDPiBeauty2CharmLine/Particles")
 
  # B0DpDmKS
  sel["B0DpDmKSLL"]                = AutomaticData(Location = locationRoot+"Phys/B02DDKSLLBeauty2CharmLine/Particles")
  sel["B0DpDmKSDD"]                = AutomaticData(Location = locationRoot+"Phys/B02DDKSDDBeauty2CharmLine/Particles")
 
  # BmDspDmKm
  sel["BmDspDmKm"]                = doPIDswap_Bm2DpX_to_Bm2DspX("BmDpDmKm","BmDspDmKm");
  
  # BmDspDsmKm
  sel["BmDspDsmKm"]               = doPIDswap_B2DpDmX_to_B2DspDsmX("BmDpDmKm","BmDspDsmKm");
  
  # BmDspDmpim
  sel["BmDspDmpim"]                = doPIDswap_Bm2DpX_to_Bm2DspX("BmDpDmpim","BmDspDmpim");
  
  # BmDspDsmpim
  sel["BmDspDsmpim"]               = doPIDswap_B2DpDmX_to_B2DspDsmX("BmDpDmpim","BmDspDsmpim");
  
  # B0D0barDpKm
  sel["B0D0DpKm"]                  = AutomaticData(Location = locationRoot+"Phys/B02D0DKBeauty2CharmLine/Particles")
  sel["B0D0barDmKp"]=sel["B0D0DpKm"] # For MC naming

  # B0D0DsmKp
  sel["B0D0DsmKp"]                = doPIDswap_B02DmD0X_to_B02DsmD0X("B0D0DmKp","B0D0DsmKp");
  
  # B0D0Dmpip
  #sel["B0D0Dmpip"]                = doBachPIDswap("B0D0DmKp","B0D0Dmpip");
  sel["B0D0Dmpip"]                = AutomaticData(Location = locationRoot+"Phys/B02D0DPiBeauty2CharmLine/Particles");
  
  # B0D0Dsmpip
  sel["B0D0Dsmpip"]               = doPIDswap_B02DmD0X_to_B02DsmD0X("B0D0Dmpip","B0D0Dsmpip");

  # B0D0DspKm
  sel["B0D0DspKm"]                = doPIDswap_B02DpD0X_to_B02DspD0X("B0D0DpKm","B0D0DspKm");
  
  # B0D0Dppim
  sel["B0D0Dppim"]                = doBachPIDswap("B0D0DpKm","B0D0Dppim");
  
  # B0D0Dsppim
  sel["B0D0Dsppim"]               = doPIDswap_B02DpD0X_to_B02DspD0X("B0D0Dppim","B0D0Dsppim");

  # B0D0barDsmKp
  sel["B0D0barDsmKp"]                = doPIDswap_B02DmD0barX_to_B02DsmD0barX("B0D0DmKp","B0D0barDsmKp"); 
  
  # B0D0barDmpip
  sel["B0D0barDmpip"]                = doBachPIDswap("B0D0DmKp","B0D0barDmpip");
  
  # B0D0barDsmpip
  sel["B0D0barDsmpip"]               = doPIDswap_B02DmD0barX_to_B02DsmD0barX("B0D0barDmpip","B0D0barDsmpip");
  
  # B0D0barDspKm
  sel["B0D0barDspKm"]                = doPIDswap_B02DpD0barX_to_B02DspD0barX("B0D0DpKm","B0D0barDspKm");
  
  # B0D0barDppim
  sel["B0D0barDppim"]                = doBachPIDswap("B0D0DpKm","B0D0barDppim");
  
  # B0D0barDsppim
  sel["B0D0barDsppim"]               = doPIDswap_B02DpD0barX_to_B02DspD0barX("B0D0barDppim","B0D0barDsppim");
  
  # BmDpDsmKm
  sel["BmDpDsmKm"]                = doPIDswap_Bm2DpDmX_to_Bm2DpDsmX("BmDpDmKm","BmDpDsmKm")
  
  # BmDspDmpim
  sel["BmDpDsmpim"]                = doPIDswap_Bm2DpDmX_to_Bm2DpDsmX("BmDpDmpim","BmDpDsmpim")

  # B0D0k3pibarDmKp
  sel["B0D0k3piDmKp"]                  = AutomaticData(Location = locationRoot+"Phys/B02D0DKD02K3PiBeauty2CharmLine/Particles")
  sel["B0D0bark3piDmKp"]=sel["B0D0k3piDmKp"] # For MC naming
  
  # B0D0k3pibarDpKm
  sel["B0D0k3piDpKm"]                  = AutomaticData(Location = locationRoot+"Phys/B02D0DKD02K3PiBeauty2CharmLine/Particles")
  sel["B0D0bark3piDpKm"]=sel["B0D0k3piDpKm"] # For MC naming

  # B0D0k3piDsmKp
  sel["B0D0k3piDsmKp"]                = doPIDswap_B02DmD0X_to_B02DsmD0X("B0D0k3piDmKp","B0D0k3piDsmKp")

  # B0D0k3piDspKm
  sel["B0D0k3piDspKm"]                = doPIDswap_B02DpD0X_to_B02DspD0X("B0D0k3piDpKm","B0D0k3piDspKm")

  # B0D0k3pibarDmpip
  sel["B0D0k3piDmpip"]                  = AutomaticData(Location = locationRoot+"Phys/B02D0DPiD02K3PiBeauty2CharmLine/Particles")
  sel["B0D0bark3piDmpip"]=sel["B0D0k3piDmpip"] # For MC naming
  
  # B0D0k3pibarDppim
  sel["B0D0k3piDppim"]                  = AutomaticData(Location = locationRoot+"Phys/B02D0DPiD02K3PiBeauty2CharmLine/Particles")
  sel["B0D0bark3piDppim"]=sel["B0D0k3piDppim"] # For MC naming

  # B0D0k3piDsmpip
  sel["B0D0k3piDsmpip"]                = doPIDswap_B02DmD0X_to_B02DsmD0X("B0D0k3piDmpip","B0D0k3piDsmpip")

  # B0D0k3piDsppim
  sel["B0D0k3piDsppim"]                = doPIDswap_B02DpD0X_to_B02DspD0X("B0D0k3piDppim","B0D0k3piDsppim")
  
  # B0D0D0barKst0
  sel["B0D0D0barKst"]               = AutomaticData(Location = locationRoot+"Phys/B02D0D0KstD02HHD02HHBeauty2CharmLine/Particles")
  sel["B0D0_K3piD0bar_K3piKst"]     = AutomaticData(Location = locationRoot+"Phys/B02D0D0KstD02K3PiD02K3PiBeauty2CharmLine/Particles")
  sel["B0D0_K3piD0barKst"]          = AutomaticData(Location = locationRoot+"Phys/B02D0D0KstD02HHD02K3PiBeauty2CharmLine/Particles")
  sel["B0D0D0bar_K3piKst"]          = AutomaticData(Location = locationRoot+"Phys/B02D0D0KstD02HHD02K3PiBeauty2CharmLine/Particles")

  seq={}
  seqList=[] # to put in the sequence
  for s in sel.keys():
    seq[s] = SelectionSequence("seq_"+s, TopSelection = sel[s] );
    seqList += [seq[s].sequence()]

  # =================================
  # Useful DTT functions
  # =================================
  def getDbranchesDict(_0, pre, d_string, _2):
    if(d_string=="D0"):           return {pre: _0+"^( [D0]cc -> K- pi+ )"+_2, pre+"_K": _0+"( [D0]cc -> ^K- pi+ )"+_2, pre+"_pi": _0+"( [D0]cc -> K- ^pi+ )"+_2}
    elif(d_string=="D0bar"):      return {pre: _0+"^( [D0]cc -> K+ pi- )"+_2, pre+"_K": _0+"( [D0]cc -> ^K+ pi- )"+_2, pre+"_pi": _0+"( [D0]cc -> K+ ^pi- )"+_2}
    if(d_string=="D0_K3pi"):      return {pre: _0+"^( [D0]cc -> K- pi+ pi+ pi-)"+_2, pre+"_K": _0+"( [D0]cc -> ^K- pi+ pi+ pi-)"+_2, pre+"_pi1": _0+"( [D0]cc -> K- ^pi+ pi+ pi-)"+_2, pre+"_pi2": _0+"( [D0]cc -> K- pi+ ^pi+ pi-)"+_2, pre+"_pi3": _0+"( [D0]cc -> K- pi+ pi+ ^pi-)"+_2,}
    elif(d_string=="D0bar_K3pi"): return {pre: _0+"^( [D0]cc -> K+ pi- pi- pi+)"+_2, pre+"_K": _0+"( [D0]cc -> ^K+ pi- pi- pi+)"+_2, pre+"_pi1": _0+"( [D0]cc -> K+ ^pi- pi- pi+)"+_2, pre+"_pi2": _0+"( [D0]cc -> K+ pi- ^pi- pi+)"+_2, pre+"_pi3": _0+"( [D0]cc -> K+ pi- pi- ^pi+)"+_2,}
    if(d_string=="Dp"):           return {pre: _0+"^( D+ -> K- pi+ pi+ )"+_2, pre+"_K": _0+"( D+ -> ^K- pi+ pi+ )"+_2, pre+"_pi1": _0+"( D+ -> K- ^pi+ pi+ )"+_2, pre+"_pi2": _0+"( D+ -> K- pi+ ^pi+ )"+_2}
    if(d_string=="Dstp"):         return {pre: _0+"^( D*(2010)+ -> ( [D0]cc -> K- pi+ ) pi+ )"+_2, pre+"_pi": _0+"( D*(2010)+ -> ( [D0]cc -> K- pi+ ) ^pi+ )"+_2, pre+"_D": _0+"( D*(2010)+ -> ^( [D0]cc -> K- pi+ ) pi+ )"+_2, pre+"_D_K": _0+"( D*(2010)+ -> ( [D0]cc -> ^K- pi+ ) pi+ )"+_2, pre+"_D_pi": _0+"( D*(2010)+ -> ( [D0]cc -> K- ^pi+ ) pi+ )"+_2}
    if(d_string=="Dstm"):         return {pre: _0+"^( D*(2010)- -> ( [D0]cc -> K+ pi- ) pi- )"+_2, pre+"_pi": _0+"( D*(2010)- -> ( [D0]cc -> K+ pi- ) ^pi- )"+_2, pre+"_D": _0+"( D*(2010)- -> ^( [D0]cc -> K+ pi- ) pi- )"+_2, pre+"_D_K": _0+"( D*(2010)- -> ( [D0]cc -> ^K+ pi- ) pi- )"+_2, pre+"_D_pi": _0+"( D*(2010)- -> ( [D0]cc -> K+ ^pi- ) pi- )"+_2}
    if(d_string=="Dm"):           return {pre: _0+"^( D- -> K+ pi- pi- )"+_2, pre+"_K": _0+"( D- -> ^K+ pi- pi- )"+_2, pre+"_pi1": _0+"( D- -> K+ ^pi- pi- )"+_2, pre+"_pi2": _0+"( D- -> K+ pi- ^pi- )"+_2}
    if(d_string=="Dsp"):          return {pre: _0+"^( D_s+ -> K- K+ pi+ )"+_2, pre+"_K1": _0+"( D_s+ -> ^K- K+ pi+ )"+_2, pre+"_K2": _0+"( D_s+ -> K- ^K+ pi+ )"+_2, pre+"_pi": _0+"( D_s+ -> K- K+ ^pi+ )"+_2}
    if(d_string=="Dsm"):          return {pre: _0+"^( D_s- -> K+ K- pi- )"+_2, pre+"_K1": _0+"( D_s- -> ^K+ K- pi- )"+_2, pre+"_K2": _0+"( D_s- -> K+ ^K- pi- )"+_2, pre+"_pi": _0+"( D_s- -> K+ K- ^pi- )"+_2}
    sys.exit();  return 'NODBRANCHFOUND' #shouldn't reach this line
  def getXbranchesDict(_0, pre, x_string):
    if(x_string=="Km"):           return {"Bach":_0+"^K- ) ]CC"}
    if(x_string=="Kp"):           return {"Bach":_0+"^K+ ) ]CC"}
    if(x_string=="pim"):          return {"Bach":_0+"^pi- ) ]CC"}
    if(x_string=="pip"):          return {"Bach":_0+"^pi+ ) ]CC"}
    if(x_string=="KS"):           return {"Bach":_0+"^( KS0 -> pi+ pi- )  ) ]CC", "Bach_pi1":_0+"( KS0 -> ^pi+ pi- )  ) ]CC", "Bach_pi2":_0+"( KS0 -> pi+ ^pi- )  ) ]CC"}
    if(x_string=="Kst"):          return {"Bach":_0+"^( K*(892)0 -> K+ pi- )  ) ]CC", "Bach_K":_0+"( K*(892)0 -> ^K+ pi- )  ) ]CC", "Bach_pi":_0+"( K*(892)0 -> K+ ^pi- )  ) ]CC"}
    if(x_string=="Phi"):          return {"Bach":_0+"^( phi(1020) -> K+ K- )  ) ]CC", "Bach_pi1":_0+"( phi(1020) -> ^K+ K- )  ) ]CC", "Bach_pi2":_0+"( phi(1020) -> K+ ^K- )  ) ]CC"}
    sys.exit(); return 'NODBRANCHFOUND' #shouldn't reach this line
  def noCar(X):
    return X.replace('^','');
  def makeDecayDict(myB, myD1, myD2, myX, doCC):
    el = { "Bm":          "[ (B- -> ",
           "B0":          "[ ([B0]cc -> ",
           "B0bar":       "[ ([B~0]cc -> ",
           "Bs0":         "[ ([B_s0]cc -> ",
           "Bs0bar":      "[ ([B_s~0]cc -> ",
           "D0":          " ^( [D0]cc -> ^K- ^pi+ ) ",
           "D0bar":       " ^( [D0]cc -> ^K+ ^pi- ) ",
           "D0_K3pi":     " ^( [D0]cc -> ^K- ^pi+ ^pi+ ^pi- ) ",
           "D0bar_K3pi":  " ^( [D0]cc -> ^K+ ^pi- ^pi- ^pi+ ) ",
           "Dp":          " ^( D+ -> ^K- ^pi+ ^pi+ ) ",
           "Dm":          " ^( D- -> ^K+ ^pi- ^pi- ) ",
           "Dsp":         " ^( D_s+ -> ^K- ^K+ ^pi+ ) ",
           "Dsm":         " ^( D_s- -> ^K+ ^K- ^pi- ) ",
           "Dstp":        " ^( D*(2010)+ -> ^( [D0]cc -> ^K- ^pi+ ) ^pi+ ) ",
           "Dstm":        " ^( D*(2010)- -> ^( [D0]cc -> ^K+ ^pi- ) ^pi- ) ",
           "Km":          " ^K- )]CC",
           "Kp":          " ^K+ )]CC",
           "pim":         " ^pi- )]CC",
           "pip":         " ^pi+ )]CC",
           "KS":          " ^( KS0 -> ^pi+ ^pi- )  ) ]CC",
           "Kst":         " ^( K*(892)0 -> ^K+ ^pi- )  ) ]CC",
           "Phi":         " ^( phi(1020) -> ^K+ ^K- )  ) ]CC"}
    # This is to return
    branchList = ["",{}] # head, branches
    # Figure out overall 'Decay' string
    branchList[0] = el[myB]+el[myD1]+el[myD2]+el[myX]
    # Set up the branch list
    branchList[1]["B"] = el[myB]+noCar(el[myD1])+noCar(el[myD2])+noCar(el[myX])
    branchList[1].update( getDbranchesDict(noCar(el[myB]),                  "D1",myD1,  noCar(el[myD2])+noCar(el[myX])) );
    branchList[1].update( getDbranchesDict(noCar(el[myB])+noCar(el[myD1]),  "D2",myD2,  noCar(el[myX])) );
    branchList[1].update( getXbranchesDict(noCar(el[myB])+noCar(el[myD1])+noCar(el[myD2]),   "X",myX) );
    
    # If shouldn't do CC, remove all the CC's here
    if(doCC==False):
      branchList[0] = branchList[0].replace('CC','').replace('[','').replace(']','').replace('cc','');
      for entry in branchList[1].keys():
        branchList[1][entry] = branchList[1][entry].replace('CC','').replace('[','').replace(']','').replace('cc','');
    
    # Hand back the dictionary
    return branchList;


  from Configurables import DecayTreeTuple, TupleToolTISTOS , TupleToolStripping, TupleToolDecay
  from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo, BackgroundCategory
  from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool
  from Configurables import LoKi__Hybrid__EvtTupleTool as LoKiEvtTupleTool
  #from DecayTreeTuple.Configuration import *
  
  def makeTuple(name,decayList,input,doCC=True):
    tup = DecayTreeTuple(name)
    tup.Decay = makeDecayDict(decayList[0],decayList[1],decayList[2],decayList[3],doCC)[0]
    tup.addBranches( makeDecayDict(decayList[0],decayList[1],decayList[2],decayList[3],doCC)[1] )
    tup.ToolList = ["TupleToolGeometry","TupleToolEventInfo"]
    tup.Inputs = [ input ]

    #=======================#
    #=== LoKi evt Variables #
    #=======================#
    lokiEvtVars={
      "Preambulo" : ['from LoKiTracks.decorators import *',
                     'from LoKiNumbers.decorators import *',
                     'from LoKiCore.functions import *'],
      "nTracks"  : "RECSUMMARY( LHCb.RecSummary.nTracks  , -1)",
      "nLong":    "RECSUMMARY( LHCb.RecSummary.nLongTracks , -1)",
      "nPVs" :    "recSummary(LHCb.RecSummary.nPVs,'Rec/Vertex/Primary')"
    }
    tool=LoKiEvtTupleTool("LoKiTool_Evt")
    preambulo = lokiEvtVars.pop("Preambulo", None);
    tool.VOID_Variables=lokiEvtVars
    tool.Preambulo=preambulo
    tup.addTool(tool)
    tup.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKiTool_Evt"]
    
    #=======================#
    #=== LoKi Variables ====#
    #=======================#
    # DOCA vars
    DOCA2Body = {
            "DOCA12" : "DOCA(1,2)"
            }
    DOCA3Body = {
            "DOCA12" : "DOCA(1,2)",
            "DOCA13" : "DOCA(1,3)",
            "DOCA23" : "DOCA(2,3)",
            }
    DOCA4Body = {
            "DOCA12" : "DOCA(1,2)",
            "DOCA13" : "DOCA(1,3)",
            "DOCA14" : "DOCA(1,4)",
            "DOCA23" : "DOCA(2,3)",
            "DOCA24" : "DOCA(2,4)",
            "DOCA34" : "DOCA(3,4)"
            }

    # DTF vars
    B_LoKivars = {
               "DTF_CHI2": "DTF_CHI2( True, strings('D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               "DTF_NDOF": "DTF_NDOF( True, strings('D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               "DTF_CHI2NDOF":"DTF_CHI2NDOF( True, strings('D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               "DTF_CHI2_mBconstr": "DTF_CHI2( True, strings('B+','B-','B0','B~0','D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               "DTF_NDOF_mBconstr": "DTF_NDOF( True, strings('B+','B-','B0','B~0','D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               "DTF_CHI2NDOF_mBconstr":"DTF_CHI2NDOF( True, strings('B+','B-','B0','B~0','D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               "DTF_CHI2_noMassConstr": "DTF_CHI2( True )",
               "DTF_NDOF_noMassConstr": "DTF_NDOF( True )",
               "DTF_CHI2NDOF_noMassConstr":"DTF_CHI2NDOF( True )",
               "DTF_D0_decayLength_mBconstr":"DTF_CTAU(1, True, strings('B+','B-','B0','B~0','D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               "DTF_D0_decayLengthErr_mBconstr":"DTF_CTAUERR(1, True, strings('B+','B-','B0','B~0','D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               "DTF_D0_0_decayLength_mBconstr":"DTF_CTAU(2, True, strings('B+','B-','B0','B~0','D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               "DTF_D0_0_decayLengthErr_mBconstr":"DTF_CTAUERR(2, True, strings('B+','B-','B0','B~0','D0','D~0','D+','D-','D_s+','D_s-','KS0') )",
               };
    for branchLab,branchString in tup.Branches.iteritems():
      for var in ['E','PX','PY','PZ','M','ID']:
        if(branchLab=="B"):
          B_LoKivars.update( {"DTF_"+branchLab+"_"+var          : "DTF_FUN ( "+var+", True, strings('D0','D~0','D+','D-','D_s+','D_s-','KS0') ) "} );
          B_LoKivars.update( {"DTF_mBconstr_"+branchLab+"_"+var : "DTF_FUN ( "+var+", True, strings('B0','B~0','B+','B-','D0','D~0','D+','D-','D_s+','D_s-','KS0') ) "} );
        else:
          B_LoKivars.update( {"DTF_"+branchLab+"_"+var          : "DTF_FUN ( CHILD("+var+", '"+branchString+"'), True, strings('D0','D~0','D+','D-','D_s+','D_s-','KS0') ) "} );
          B_LoKivars.update( {"DTF_mBconstr_"+branchLab+"_"+var : "DTF_FUN ( CHILD("+var+", '"+branchString+"'), True, strings('B0','B~0','B+','B-','D0','D~0','D+','D-','D_s+','D_s-','KS0') ) "} );
    lokiVars={"B": B_LoKivars}
   
    # Loop over branches and construct variable list
    for branch in tup.Branches.keys():
      if branch not in lokiVars.keys():  lokiVars[branch]={"Preambulo":["from LoKiTracks.decorators import *", 
                                                                       "from LoKiProtoParticles.decorators import *"]}
      if branch=="B":
        lokiVars[ branch ]["BPVDIRA"]="BPVDIRA"
        lokiVars[ branch ]["BPVIPCHI2"]="BPVIPCHI2()"
        lokiVars[ branch ]["ptasy_1.50"] = "INFO(9000+11,0.)"
      
      # For all particles
      lokiVars[ branch ]["M"]="M";
      lokiVars[ branch ]["PX"]="PX";
      lokiVars[ branch ]["PY"]="PY";
      lokiVars[ branch ]["PZ"]="PZ";
      lokiVars[ branch ]["E"]="E";
      lokiVars[ branch ]["Q"]="Q";
      lokiVars[ branch ]["IPCHI2_BPV"]="BPVIPCHI2()";
      lokiVars[ branch ]["P"]="P";
      lokiVars[ branch ]["PT"]="PT";
      
      # For all non-stable particles, including the solo particle if it is KS, phi or K*
      if branch in ["B","D1","D2","D1_D","D2_D","Bach"]:
        if(branch != "Bach"):
          lokiVars[ branch ]["ENDVTX_CHI2"]="VFASPF(VCHI2)";
          lokiVars[ branch ]["FDCHI2_BPV"] ="BPVVDCHI2";
        elif (tup.Decay.find("KS0")!=-1 or tup.Decay.find("K*(892)")!=-1 or tup.Decay.find("phi(1020)")!=-1):
          lokiVars[ branch ]["ENDVTX_CHI2"]="VFASPF(VCHI2)";

        # adding DOCAMAX and DOCA for D0D0barKst tuples
        if "Kst" in name:
          lokiVars[ branch ]["DOCAMAX"]="DOCAMAX"
          if branch == "B":
            for k, v in DOCA3Body.items():
                lokiVars[ branch ][ k ] = v
          if branch == "Bach":
            for k, v in DOCA2Body.items():
                lokiVars[ branch ][ k ] = v
          if branch == "D1":
            if "K3pi" in decayList[1]:
              for k, v in DOCA4Body.items():
                lokiVars[ branch ][ k ] = v
            else:
              for k, v in DOCA2Body.items():
                lokiVars[ branch ][ k ] = v
          if branch == "D2":
            if "K3pi" in decayList[2]:
              for k, v in DOCA4Body.items():
                lokiVars[ branch ][ k ] = v
            else:
              for k, v in DOCA2Body.items():
                lokiVars[ branch ][ k ] = v
      
      # For all D products
      if branch in ["D1_K","D1_pi","D1_K1","D1_K2","D1_pi1","D1_pi2","D1_pi3",
                    "D2_K","D2_pi","D2_K1","D2_K2","D2_pi1","D2_pi2","D2_pi3",
                    "D1_D_K","D1_D_pi","D1_D_K1","D1_D_K2","D1_D_pi1","D1_D_pi2","D1_D_pi3",
                    "D2_D_K","D2_D_pi","D2_D_K1","D2_D_K2","D2_D_pi1","D2_D_pi2","D2_D_pi3",
                    "Bach","Bach_pi","Bach_K","Bach_pi1","Bach_pi2","Bach_K1","Bach_K2"]:
        lokiVars[ branch ]["PIDK"]="PIDK"
        lokiVars[ branch ]["PIDp"]="PIDp"
        lokiVars[ branch ]["ProbNNK"]="PROBNNk"
        lokiVars[ branch ]["ProbNNpi"]="PROBNNpi"
        lokiVars[ branch ]["ProbNNp"]="PROBNNp"
        lokiVars[ branch ]["ProbNNmu"]="PROBNNmu"
        lokiVars[ branch ]["TRCHI2DOF"]="TRCHI2DOF"
        lokiVars[ branch ]["Tr_GhostProb"] = "TRGHP"
        lokiVars[ branch ]["hasRich"] = "switch( PPCUT(PP_HASRICHPID),1 , 0)"
        lokiVars[ branch ]["RiThresholdPi"]   = "switch( PPCUT(PP_RICHTHRES_PI),1 , 0)"
        lokiVars[ branch ]["RiThresholdKa"]   = "switch( PPCUT(PP_RICHTHRES_K),1 , 0)"
        if branch != 'Bach':
          lokiVars[ branch ]["isMu"]            = "switch( ISMUON,1 , 0)"

    conf_toolLists={}
    # Loop over the Tuples branches, and if find them in the LoKi variables dictionary, add the appropriate variables
    for branch in tup.Branches.keys():
      dec = TupleToolDecay(branch)
      if branch in lokiVars:
        tool = LoKiTupleTool("LoKiTool_%s" %branch);
        preambulo = lokiVars[branch].pop("Preambulo", None);
        tool.Variables = lokiVars[branch]
        if preambulo is not None:
          tool.Preambulo += preambulo
        dec.addTool(tool)
        dec.ToolList=["LoKi::Hybrid::TupleTool/LoKiTool_%s" %branch]
      if branch in conf_toolLists:
        dec.ToolList+=conf_toolLists(branch)
      tup.addTool(dec)

    # Add TIS TOS info to the B
    tup.B.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
    tup.B.TupleToolTISTOS.VerboseL0=True
    tup.B.TupleToolTISTOS.VerboseHlt1=True
    tup.B.TupleToolTISTOS.VerboseHlt2=True
    tup.B.TupleToolTISTOS.TriggerList= mytriggers
    tup.addTupleTool(TupleToolStripping, name="TupleToolStripping");
    tup.TupleToolStripping.StrippingList = mystrips

    # TIS TOS for all daughters
    if "Kst" in name:
        if hasattr(tup, 'Bach_K'):
            tup.Bach_K.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.Bach_K.TupleToolTISTOS.VerboseL0 = True
            tup.Bach_K.TupleToolTISTOS.TriggerList = mytriggers
        if hasattr(tup, 'Bach_pi'):
            tup.Bach_pi.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.Bach_pi.TupleToolTISTOS.VerboseL0 = True
            tup.Bach_pi.TupleToolTISTOS.TriggerList = mytriggers
        if hasattr(tup, 'D1_K'):
            tup.D1_K.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D1_K.TupleToolTISTOS.VerboseL0 = True
            tup.D1_K.TupleToolTISTOS.TriggerList = mytriggers
        if hasattr(tup, 'D2_K'):
            tup.D2_K.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D2_K.TupleToolTISTOS.VerboseL0 = True
            tup.D2_K.TupleToolTISTOS.TriggerList = mytriggers
        # Different TISTOS vars dependednt on which D -> K3pi
        if hasattr(tup, 'D1_pi'):
            tup.D1_pi.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D1_pi.TupleToolTISTOS.VerboseL0 = True
            tup.D1_pi.TupleToolTISTOS.TriggerList = mytriggers
        elif hasattr(tup, 'D1_pi1'):
            tup.D1_pi1.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D1_pi1.TupleToolTISTOS.VerboseL0 = True
            tup.D1_pi1.TupleToolTISTOS.TriggerList = mytriggers
            tup.D1_pi2.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D1_pi2.TupleToolTISTOS.VerboseL0 = True
            tup.D1_pi2.TupleToolTISTOS.TriggerList = mytriggers
            tup.D1_pi3.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D1_pi3.TupleToolTISTOS.VerboseL0 = True
            tup.D1_pi3.TupleToolTISTOS.TriggerList = mytriggers
        if hasattr(tup, 'D2_pi'):
            tup.D2_pi.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D2_pi.TupleToolTISTOS.VerboseL0 = True
            tup.D2_pi.TupleToolTISTOS.TriggerList = mytriggers
        elif hasattr(tup, 'D2_pi1'):
            tup.D2_pi1.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D2_pi1.TupleToolTISTOS.VerboseL0 = True
            tup.D2_pi1.TupleToolTISTOS.TriggerList = mytriggers
            tup.D2_pi2.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D2_pi2.TupleToolTISTOS.VerboseL0 = True
            tup.D2_pi2.TupleToolTISTOS.TriggerList = mytriggers
            tup.D2_pi3.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
            tup.D2_pi3.TupleToolTISTOS.VerboseL0 = True
            tup.D2_pi3.TupleToolTISTOS.TriggerList = mytriggers


    # If dealing with MC, add truth-matching information
    if isSimulation:
        tup.ToolList += ["TupleToolMCTruth"]
        tup.addTool(TupleToolMCTruth())
        tup.TupleToolMCTruth.ToolList = [ 
                         "MCTupleToolDecayType" ,
                         "MCTupleToolHierarchy" ,
                         "MCTupleToolKinematic" ,
                         "MCTupleToolReconstructed" ]

    # add BKGCAT to the B for D0D0barKst tuples
        if "Kst" in name:
            tup.B.addTool(TupleToolMCBackgroundInfo)
            tup.B.TupleToolMCBackgroundInfo.addTool(BackgroundCategory)
            tup.B.ToolList += ["TupleToolMCBackgroundInfo"]

    
    return tup;

  # ================================
  # Make the tuples
  # ================================
  tupleList=[]
  #tupleList+=[ makeTuple("BmD0D0barKm",   ["Bm","D0","D0bar","Km"],                   seq["BmD0D0barKm"].outputLocation() ) ]   
  #tupleList+=[ makeTuple("BmD0_K3piD0bar_K3piKm", ["Bm","D0_K3pi","D0bar_K3pi","Km"], seq["BmD0_K3piD0bar_K3piKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmD0_K3piD0barKm",      ["Bm","D0_K3pi","D0bar","Km"],      seq["BmD0_K3piD0barKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmD0D0bar_K3piKm",      ["Bm","D0","D0bar_K3pi","Km"],      seq["BmD0D0bar_K3piKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmDpDmKm",      ["Bm","Dp","Dm","Km"],                    seq["BmDpDmKm"].outputLocation()    ) ]  
  #tupleList+=[ makeTuple("B0D0barDpKm",           ["B0","D0",     "Dm","Kp"],         seq["B0D0DmKp"].outputLocation()    ) ]
  #tupleList+=[ makeTuple("B0D0bar_K3piDpKp",      ["B0","D0_K3pi","Dm","Kp"],         seq["B0D0_K3piDmKp"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmD0DmKSLL",    ["Bm","D0","Dm","KS"],                    seq["BmD0DmKSLL"].outputLocation()  ) ]
  #tupleList+=[ makeTuple("BmD0DmKSDD",    ["Bm","D0","Dm","KS"],                    seq["BmD0DmKSDD"].outputLocation()  ) ]
  #tupleList+=[ makeTuple("B0D0D0barKSLL", ["B0","D0","D0bar","KS"],                 seq["B0D0D0barKSLL"].outputLocation()  ) ]
  #tupleList+=[ makeTuple("B0D0D0barKSDD", ["B0","D0","D0bar","KS"],                 seq["B0D0D0barKSDD"].outputLocation()  ) ]
  #tupleList+=[ makeTuple("BmDpDmpim",     ["Bm","Dp","Dm","pim"],                   seq["BmDpDmpim"].outputLocation()    ) ]
  #tupleList+=[ makeTuple("B0DpDmKSLL",    ["B0","Dp","Dm","KS"],                    seq["B0DpDmKSLL"].outputLocation()  ) ]
  #tupleList+=[ makeTuple("B0DpDmKSDD",    ["B0","Dp","Dm","KS"],                    seq["B0DpDmKSDD"].outputLocation()  ) ]
  
  #B->D Ds h
  #tupleList+=[ makeTuple("BmDspDmKm",     ["Bm","Dsp","Dm","Km"],                   seq["BmDspDmKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmDspDsmKm",    ["Bm","Dsp","Dsm","Km"],                  seq["BmDspDsmKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmDspDmpim",    ["Bm","Dsp","Dm","pim"],                  seq["BmDspDmpim"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmDspDsmpim",   ["Bm","Dsp","Dsm","pim"],                 seq["BmDspDsmpim"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmDpDsmKm",     ["Bm","Dp","Dsm","Km"],                   seq["BmDpDsmKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("BmDpDsmpim",    ["Bm","Dp","Dsm","pim"],                  seq["BmDpDsmpim"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0k3piDsmKp",    ["B0","D0_K3pi","Dsm","Kp"],                  seq["B0D0k3piDsmKp"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0k3piDspKm",    ["B0","D0_K3pi","Dsp","Km"],                  seq["B0D0k3piDspKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0k3piDsmpip",    ["B0","D0_K3pi","Dsm","pip"],                  seq["B0D0k3piDsmpip"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0k3piDsppim",    ["B0","D0_K3pi","Dsp","pim"],                  seq["B0D0k3piDsppim"].outputLocation() ) ]
  
  #B0->D0(bar) Ds h
  #tupleList+=[ makeTuple("B0D0DsmKp",     ["B0","D0","Dsm","Kp"],                   seq["B0D0DsmKp"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0Dsmpip",    ["B0","D0","Dsm","pip"],                  seq["B0D0Dsmpip"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0DspKm",     ["B0","D0","Dsp","Km"],                   seq["B0D0DspKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0Dsppim",    ["B0","D0","Dsp","pim"],                  seq["B0D0Dsppim"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0barDsmKp",  ["B0","D0bar","Dsm","Kp"],                seq["B0D0barDsmKp"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0barDsmpip", ["B0","D0bar","Dsm","pip"],               seq["B0D0barDsmpip"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0barDspKm",  ["B0","D0bar","Dsp","Km"],                seq["B0D0barDspKm"].outputLocation() ) ]
  #tupleList+=[ makeTuple("B0D0barDsppim", ["B0","D0bar","Dsp","pim"],               seq["B0D0barDsppim"].outputLocation() ) ]

  # B0 -> D0 D0bar K+ pi-
  tupleList+=[ makeTuple("B0D0D0barKst",              ["B0","D0","D0bar","Kst"],                 seq["B0D0D0barKst"].outputLocation() ) ]
  tupleList+=[ makeTuple("B0D0_K3piD0bar_K3piKst",    ["B0","D0_K3pi","D0bar_K3pi","Kst"],       seq["B0D0_K3piD0bar_K3piKst"].outputLocation() ) ]
  tupleList+=[ makeTuple("B0D0_K3piD0barKst",         ["B0","D0_K3pi","D0bar","Kst"],            seq["B0D0_K3piD0barKst"].outputLocation() ) ]
  tupleList+=[ makeTuple("B0D0D0bar_K3piKst",         ["B0","D0","D0bar_K3pi","Kst"],            seq["B0D0D0bar_K3piKst"].outputLocation() ) ]

  # ================================
  # Event tuple for simulation only
  # ================================
  from Configurables import EventTuple
  etuple = EventTuple()
  etuple.ToolList = ["TupleToolEventInfo"]
  if isSimulation == True :
      tupleList += [ etuple ]

  # ================================
  # Track scale for data
  # ================================
  from Configurables import TrackScaleState as SCALER
  scaler=SCALER('Scaler')
  from Configurables import TrackSmearState as SMEAR
  smear = SMEAR('Smear')
  
  seqToReturn = None

  # Scale if data, smear if simulation
  if isSimulation == True :
    seqToReturn = [ smear ]  + seqList + tupleList
  else :
    seqToReturn = [ scaler ] + seqList + tupleList 

  return seqToReturn

