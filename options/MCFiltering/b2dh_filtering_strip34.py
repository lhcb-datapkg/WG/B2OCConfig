"""
@author Lennaert Bel
@date   2017-03-30

Margarete Schellenberg, 13/12/2017
copy of b2dh_filtering_strip28.py 
Stripping version S28 -> S28r1 
StrippingTCK 0x41432800 --> 0x41442810

Alessandro Bertolin, 25/1/2019
copy of b2dh_filtering_strip28r1.py
Stripping version S28r1 -> S29r2
StrippingTCK 0x41442810 --> 0x42722920

Alessandro Bertolin, 31/1/2019
copy of b2dh_filtering_strip29r2.py
Stripping version S29r2 -> S34
StrippingTCK 0x42722920 --> 0x44403400
"""

year = 2018
assert(year in [2015, 2016, 2017, 2018])

from Gaudi.Configuration import *
MessageSvc().Format = "% F%40W%S%20W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping = {2015: 'stripping24', 2016: 'stripping28r1', 2017: 'stripping29r2', 2018: 'stripping34'}[year]

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

#
# Merge into one stream and run in filtering mode
#
MyStream = StrippingStream("B2DH.Strip")

# Select lines by name
MyLines = ['StrippingB02DPiD2HHHBeauty2CharmLine', 'StrippingB02DKD2HHHBeauty2CharmLine']

# Remove prescale and append the line to the stream
for stream in streams:
    if 'BhadronCompleteEvent' in stream.name():
        for line in stream.lines:
            if line.name() in MyLines:
                line._prescale = 1.
                MyStream.appendLines([line])


# Configure Stripping
sc = StrippingConf(Streams = [MyStream], TESPrefix = 'Strip')
MyStream.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

###########################################
###########################################



from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter, stripDSTStreamConf, stripDSTElements)

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = { 'default': stripDSTElements(pack=True) }

SelDSTWriterConf = { 'default': stripDSTStreamConf(pack=True) }

for stream in sc.activeStreams() :
       print("there is a stream called " + stream.name() + " active")
       dstWriter = SelDSTWriter("MyDSTWriter",
                                StreamConf = SelDSTWriterConf,
                                MicroDSTElements = SelDSTWriterElements,
                                OutputFileSuffix = '000000',
                                SelectionSequences = sc.activeStreams()
                                )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK = {2015: 0x38112400, 2016: 0x41442810, 2017: 0x42722920, 2018: 0x44403400}[year])

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().DataType = str(year)
DaVinci().ProductionType = "Stripping"

DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([dstWriter.sequence()])
