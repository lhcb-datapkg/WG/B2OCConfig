
# Helper file to define data type for davinci

the_year      = '2011'
the_fileType  = 'DST'
the_rootintes = ""
the_locationRoot = "/Event/BhadronCompleteEvent/"

from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
               RootInTES                 = the_rootintes      ,
             )

from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from b2oc_aman_b2ddhDalitz.selectB2DDh import configure_b2oc_aman_ddhDalitz_selection
dv.appendToMainSequence( configure_b2oc_aman_ddhDalitz_selection( dataYear = the_year , locationRoot = the_locationRoot ) )

db = CondDB  ( LatestGlobalTagByDataType = the_year )

if '__main__' == __name__ :

  print ' The year : ', the_year

