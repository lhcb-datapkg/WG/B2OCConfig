# ================================
# Read only fired events for speed
# ================================

from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters

fltrs = LoKi_Filters (
        STRIP_Code="""
        HLT_PASS('StrippingB2D0D0KBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2D0D0KD02HHD02HHBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2D0D0KD02HHD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2DstDKDstarD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2D0D0KD02K3PiD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB02DstD0KDstarD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB02DstD0KD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB02D0DKBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB02D0DKD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2DstDKBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2Dst0D0KDst02D0GammaD02HHBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB02DstD0KBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2Dst0D0KDst02D0GammaD02HHBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2Dst0D0KDst02D0Pi0ResolvedD02HHBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2DstDPiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2DstDPiDstarD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS_RE('Hlt2Topo.*Decision') | \
        HLT_PASS_RE('Hlt2*IncPhi.*Decision') """
        )

# ================================
# Configure DaVinci
# ================================

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci

davinci = DaVinci (
    EventPreFilters = fltrs.filters ('Filters') ,
    EvtMax          =     -1    ,
    PrintFreq       =   1000    ,
    Lumi            =   True    ,
    TupleFile       = "B2OC_AMAN_B2DSTDSHDALITZSEL.ROOT"
    )
