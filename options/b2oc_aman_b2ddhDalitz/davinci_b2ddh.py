# ================================
# Read only fired events for speed
# ================================

from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS('StrippingB02D0D0KstD02HHD02HHBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KstD02HHD02K3PiBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KstD02K3PiD02K3PiBeauty2CharmLineDecision') """
    )
#    HLT_PASS('StrippingB02D0DKBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB02D0DKD02K3PiBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB2D0D0KBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB2D0D0KD02HHD02HHBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB2D0D0KD02K3PiD02K3PiBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB2D0D0KD02HHD02K3PiBeauty2CharmLineDecision') """
#    HLT_PASS('StrippingB02D0DPiBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB2DDKBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB2D0DKSLLBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB2D0DKSDDBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB02D0D0KSD02HHD02HHLLBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB02D0D0KSD02HHD02HHDDBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB2DDPiBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB02DDKSDDBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB02D0DPiD02K3PiBeauty2CharmLineDecision') | \
#    HLT_PASS('StrippingB02DDKSLLBeauty2CharmLineDecision') """
# ================================
# Configure DaVinci
# ================================

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci

davinci = DaVinci (
    EventPreFilters = fltrs.filters ('Filters') ,
    EvtMax          =     -1    ,
    PrintFreq       =   1000    ,
    Lumi            =   True    ,
    TupleFile       = "B2OC_AMAN_B2DDHDALITZSEL.ROOT"
    )

