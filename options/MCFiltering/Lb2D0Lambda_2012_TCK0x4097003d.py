stripping = 'stripping21'

tck12a = '0x4097003d'
tck12b = '0x409F0045'
tck = tck12a

outputFileSuffix = 'filtered'

from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
# Compare __init__.py at 
# https://svnweb.cern.ch/trac/lhcb/browser/DBASE/trunk/RawEventFormat/python/RawEventFormat/
# for finding
# - Input = 2.0 <-> multiple-TCK DSTs
# - Output = 4.0 <-> Stripping21
from Configurables import RawEventJuggler
RawEventJuggler().Input  = 3.0
RawEventJuggler().Output = 4.0
RawEventJuggler().DataOnDemand = True
RawEventJuggler().TCK = tck

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive
config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)
streams = buildStreams(stripping=config, archive=archive)

allStreams = StrippingStream('Lb2D0Lambda.MC12aStrip')
for stream in streams:
    #print stream.name()
    if stream.name() == 'Bhadron':
        lines = []
        for line in stream.lines:
            if 'Lb2D0Lambda0' in line.name():
                lines.append(line)
        allStreams.appendLines(lines)

sc = StrippingConf(Streams=[allStreams], MaxCandidates=2000, TESPrefix='Strip')

# True: Get all events written out
# False: Get only events selected by our stripping lines written out
allStreams.sequence().IgnoreFilterPassed = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements
SelDSTWriterElements = {'default' : stripDSTElements(pack=True)}
SelDSTWriterConf = {'default' : stripDSTStreamConf(pack=True, selectiveRawEvent=False)}

# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ['/Event/pRec/ProtoP#99', '/Event/pRec/Calo#99']
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter('MyDSTWriter',
    StreamConf = SelDSTWriterConf,
    MicroDSTElements = SelDSTWriterElements,
    OutputFileSuffix = outputFileSuffix,
    SelectionSequences = sc.activeStreams())

# Add stripping report
from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections(), ReportFrequency = 5000)

# Add stripping TCK (0x36112100 -> DV36r1p1 & Stripping21)
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36112100)

# Rejection Factor
from Configurables import DumpFSR, DaVinci
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = 'dumpfsr.' + outputFileSuffix + '.txt'
#DaVinci().MoniSequence += [DumpFSR()]
#from Configurables import DaVinci

DaVinci().Simulation = True
DaVinci().HistogramFile = 'DVHistos.root'
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([sr]) # do this first
DaVinci().appendToMainSequence([stck]) # and this afterwards
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = 'Stripping'

#DaVinci().EvtMax = -1
#DaVinci().InputType = 'DST'
#DaVinci().PrintFreq = 10000
#DaVinci().DataType = '2012'
#DaVinci().Lumi = not DaVinci().Simulation
#DaVinci().EvtMax = -1
#DaVinci().CondDBtag = 'sim-20160321-2-vc-md100'
#DaVinci().DDDBtag = 'dddb-20150928'

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name='TIMER')
TimingAuditor().TIMER.NameSize = 60
