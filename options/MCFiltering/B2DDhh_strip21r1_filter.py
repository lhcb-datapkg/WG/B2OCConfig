"""
Stripping 21r1 for B2DDhh in DST
@author Linxuan Zhu
@date   2023-05-16
"""
##################################################################
# ONLY THESE SECTIONS NEED TO BE MODIFIED FOR STANDARD FILTERING
##################################################################

#stripping version
##################################################################
stripping='stripping21r1'
##################################################################

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)
 
from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"
 
#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )
#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive
 
 
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)
 
streams = buildStreams(stripping = config, archive = archive)
# Select my lines
#OUTPUT STREAM NAME
##################################################################
AllStreams = StrippingStream("B2DDhh.Strip")			 
##################################################################
linesToAdd = []

#LIST OF FILTERED LINES
##################################################################
MyLines = [ 'StrippingB02DDKstBeauty2CharmLine',
            'StrippingB2DD0KstBeauty2CharmLine',
            'StrippingB02DstDKstBeauty2CharmLine',
            'StrippingCC2DDLine']
##################################################################

 
for stream in streams:
    if 'Bhadron' in stream.name():
        for line in stream.lines:
            line._prescale = 1.0
            if line.name() in MyLines:
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)
 
 
 
sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )
 
 
AllStreams.sequence().IgnoreFilterPassed = False # so that we do not get all events written out
 
#
 
# Configuration of SelDSTWriter
#
#STANDARD CONFIGURATION FOR FULL DST OUTPUT 
#(change this for the following commented section for mDST output)
##################################################################
enablePacking = True 
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
 
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }
 
SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }
##################################################################

"""
#STANDARD CONFIGURATION FOR MDST OUTPUT
##################################################################
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf 
                                      )

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
    }
##################################################################
"""

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
 
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs
 
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )
 
# Add stripping TCK
from Configurables import StrippingTCK

#TCK IN THE FOLLOWING FORMAT: DaVinci version (XXrXpX) Stripping version (XXrXpX) (i.e. DaVinci 41r4p5 with Stripping28r1p1 -> TCK:41452811)
#you can find the associated DaVinci version for each stripping in: https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses
##################################################################
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36112110)
##################################################################

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                     # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
 
# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60


"""
#Testing on local data
##################################################################
from GaudiConf import IOHelper
DaVinci().DataType = '2011'
IOHelper().inputFiles([
    '2011/B2DD0KK3PiPHSP/00039303_00000008_2.AllStreams.dst'
], clear=True)
##################################################################
"""


