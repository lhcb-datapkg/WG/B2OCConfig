get access URLs:
2011 BHADRON : lb-dirac dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision11/BHADRON.MDST/00041838/0000/00041838_00000259_1.bhadron.mdst
root://lobster1.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/LHCb/Collision11/BHADRON.MDST/00041838/0000/00041838_00000259_1.bhadron.mdst
#dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision11/BHADRON.MDST/00041838/0000/00041838_00000383_1.bhadron.mdst
#root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision11/BHADRON.MDST/00041838/0000/00041838_00000383_1.bhadron.mdst

2011 BHADRONCOMPLETEEVENT : lb-dirac dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision11/BHADRONCOMPLETEEVENT.DST/00041838/0000/00041838_00000467_1.bhadroncompleteevent.dst
root://x509up_u32781@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/LHCb/Collision11/BHADRONCOMPLETEEVENT.DST/00041838/0000/00041838_00000467_1.bhadroncompleteevent.dst
#dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision11/BHADRONCOMPLETEEVENT.DST/00041838/0000/00041838_00000739_1.bhadroncompleteevent.dst
#root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision11/BHADRONCOMPLETEEVENT.DST/00041838/0000/00041838_00000739_1.bhadroncompleteevent.dst

2012 BHADRON : lb-dirac dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision12/BHADRON.MDST/00041834/0000/00041834_00000468_1.bhadron.mdst
root://x509up_u32781@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/BHADRON.MDST/00041834/0000/00041834_00000468_1.bhadron.mdst
#dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision12/BHADRON.MDST/00041834/0000/00041834_00000057_1.bhadron.mdst
#root://clhcbstager.ads.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/LHCb/Collision12/BHADRON.MDST/00041834/0000/00041834_00000057_1.bhadron.mdst?svcClass=lhcbDst

2012 BHADRONCOMPLETEEVENT : lb-dirac dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041834/0000/00041834_00000778_1.bhadroncompleteevent.dst
root://x509up_u32781@xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041834/0000/00041834_00000778_1.bhadroncompleteevent.dst
#dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041834/0000/00041834_00000895_1.bhadroncompleteevent.dst
#root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041834/0000/00041834_00000895_1.bhadroncompleteevent.dst

2015 BHADRON : lb-dirac dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision15/BHADRON.MDST/00102356/0000/00102356_00000729_1.bhadron.mdst
root://x509up_u32781@marsedpm.in2p3.fr//dpm/in2p3.fr/home/lhcb/LHCb/Collision15/BHADRON.MDST/00102356/0000/00102356_00000729_1.bhadron.mdst
#dirac-dms-lfn-accessURL --Protocol=xroot,root  /lhcb/LHCb/Collision15/BHADRON.MDST/00049671/0000/00049671_00000193_1.bhadron.mdst
#root://f01-080-125-e.gridka.de:1094/pnfs/gridka.de/lhcb/LHCb/Collision15/BHADRON.MDST/00049671/0000/00049671_00000193_1.bhadron.mdst

2016 BHADRON : lb-dirac dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision16/BHADRON.MDST/00103102/0000/00103102_00000417_1.bhadron.mdst
root://x509up_u32781@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/LHCb/Collision16/BHADRON.MDST/00103102/0000/00103102_00000417_1.bhadron.mdst
#dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision16/BHADRON.MDST/00070442/0000/00070442_00003800_1.bhadron.mdst
#root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/BHADRON.MDST/00070442/0000/00070442_00003800_1.bhadron.mdst

2017 BHADRON : lb-dirac dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision17/BHADRON.MDST/00071671/0000/00071671_00000179_1.bhadron.mdst
root://x509up_u32781@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/BHADRON.MDST/00071671/0000/00071671_00000179_1.bhadron.mdst
#dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision17/BHADRON.MDST/00071957/0000/00071957_00000011_1.bhadron.mdst
#root://marsedpm.in2p3.fr:1094//dpm/in2p3.fr/home/lhcb/LHCb/Collision17/BHADRON.MDST/00071957/0000/00071957_00000011_1.bhadron.mdst

2018 BHADRON : lb-dirac dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision18/BHADRON.MDST/00076476/0000/00076476_00003468_1.bhadron.mdst
root://storage01.lcg.cscs.ch:1094/pnfs/lcg.cscs.ch/lhcb/lhcb/LHCb/Collision18/BHADRON.MDST/00076476/0000/00076476_00003468_1.bhadron.mdst
#dirac-dms-lfn-accessURL --Protocol=xroot,root /lhcb/LHCb/Collision18/BHADRON.MDST/00076476/0000/00076476_00000013_1.bhadron.mdst
#root://ccdcacli265.in2p3.fr:1094/pnfs/in2p3.fr/data/lhcb/LHCb/Collision18/BHADRON.MDST/00076476/0000/00076476_00000013_1.bhadron.mdst 
