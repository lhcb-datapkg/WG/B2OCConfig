"""
Stripping filtering file for Lb2Lcpipi0 MC
@author: Lanling Lu (lanlinglu@cern.ch)
@data 2024-06-11
DaVinci Version : DaVinciDev_v44r10p5
"""

stripping='stripping24r2'

#from CommonParticlesArchive import CommonParticlesArchiveConf
#CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from CommonParticles.Utils import *
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
                    "CloneDistCut" : [5000, 9e+99 ] }


# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)
streams = buildStreams(stripping = config, archive = archive)

AllStreams = StrippingStream("Lb2Lcpipi0.Strip")
listToAdd = []
MyLines = ['StrippingInclusiveCharmBaryons_LcLine']
linesToAdd = []

# turn off all pre-scalings
for stream in streams:
    if 'BhadronCompleteEvent' in stream.name():
        for line in stream.lines:
            line._prescale = 1.0
            if line.name() in MyLines:
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)


# Merge into one stream and run in flag mode


sc = StrippingConf(Streams=[AllStreams],
                   MaxCandidates=2000,
                   TESPrefix='Strip')

AllStreams.sequence().IgnoreFilterPassed = False # if True, we get all events written out


#Now apply selection
from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand, MergedSelection, MultiSelectionSequence
from Configurables          import FilterDesktop
from Configurables          import CombineParticles, CheckPV
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKiSvc
from Configurables import FilterInTrees, FilterDecays
from CommonMCParticles import StandardMCPions


_MyLcline= DataOnDemand(Location="/Event/Phys/InclusiveCharmBaryons_LcLine/Particles")
_selLc = FilterDesktop('selLc')
_selLc.Code = ("P > 0.*MeV")
MyLcSel = Selection("MyLcSel", Algorithm=_selLc, RequiredSelections=[_MyLcline])
MyLcSequence = SelectionSequence("MyLcSequence", TopSelection = MyLcSel)

_stdPi = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
_selPi = FilterDesktop('selPi')
_selPi.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selPi.Code = (" (mcMatch('[pi-]cc'))  & (PT > 300.*MeV) & (TRCHI2DOF < 4.) ")
MyPiSel = Selection("MyPiSel", Algorithm=_selPi, RequiredSelections = [_stdPi])
MyPiSequence = SelectionSequence("MyPiSequence", TopSelection = MyPiSel)

#
_stdrepi0 = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")
_selrePi0 = FilterDesktop('selrePi0')
_selrePi0.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selrePi0.Code = ("(mcMatch('[pi0]cc')) & (PT > 300*MeV) & (M >100*MeV) & (M < 170*MeV)")
MyrePi0Sel = Selection("MyrePi0Sel", Algorithm=_selrePi0, RequiredSelections = [_stdrepi0])
MyrePi0Sequence = SelectionSequence("MyrePi0Sequence", TopSelection = MyrePi0Sel)

_stdmePi0 = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")
_selmePi0 = FilterDesktop('selmePi0')
_selmePi0.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selmePi0.Code = ("(mcMatch('[pi0]cc')) & (PT > 500*MeV) & (M > 75*MeV) & (M < 195*MeV)") 
MymePi0Sel = Selection("MymePi0Sel", Algorithm = _selmePi0, RequiredSelections = [ _stdmePi0 ])
MymePi0Sequence = SelectionSequence("MymePi0Sequence", TopSelection = MymePi0Sel)

#####combine
_makeLb = CombineParticles("make_Lb",
     DecayDescriptor = "[Lambda_b0 -> Lambda_c+  pi- pi0]cc",
     CombinationCut =  " (AM<7000*MeV) & (AM>4000*MeV)",
     MotherCut = "((mcMatch('[Lambda_b0 ==> Lambda_c+  pi- pi0]CC')) & (VFASPF(VCHI2) < 25)  & ( BPVIPCHI2() < 50 ) & (BPVDIRA > 0.999) )" ) 
#include intermediate resonances


selLbR = Selection("SelLbR",
          Algorithm = _makeLb,
          RequiredSelections = [ MyLcSel, MyPiSel, MyrePi0Sel] )
seqR = SelectionSequence("MCTrueR", TopSelection=selLbR )

               
selLbM = Selection("SelLbM",
          Algorithm = _makeLb,
          RequiredSelections = [MyLcSel, MyPiSel, MymePi0Sel] )
seqM = SelectionSequence("MCTrueM", TopSelection=selLbM )

AllPi0 = MultiSelectionSequence("Lb2Lcpipi0.MCTruth", Sequences = [seqR, seqM])




# Configuration of SelDSTWriter
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements

SelDSTWriterElements = {'default' : stripDSTElements(pack=enablePacking)}

SelDSTWriterConf = {'default': stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=False,fileExtension='.dst')}

caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Filtered',
                         SelectionSequences=[AllPi0])

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44a52420)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([MyLcSequence.sequence()])
DaVinci().appendToMainSequence([MyPiSequence.sequence()])
DaVinci().appendToMainSequence([MyrePi0Sequence.sequence()])
DaVinci().appendToMainSequence([MymePi0Sequence.sequence()])
DaVinci().appendToMainSequence([seqR.sequence()])
DaVinci().appendToMainSequence([seqM.sequence()])
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = "Stripping"

#options
DaVinci().Simulation = True
DaVinci().InputType = 'DST'
DaVinci().DataType = "2015"
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60


############local test###########
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt_2015"
#DaVinci().MoniSequence += [DumpFSR()]


#########Lcrho##############
#from GaudiConf import IOHelper
#IOHelper().inputFiles([
#'/afs/ihep.ac.cn/users/l/lulanling/5T/DaVinci/LbLcpipi0/dst/2016/00073174_00000005_7.AllStreams.dst',
#'/afs/ihep.ac.cn/users/l/lulanling/5T/DaVinci/LbLcpipi0/dst/2016/00073174_00000002_7.AllStreams.dst'], clear=True)
############################

######Sigmacpi-###########
#from GaudiConf import IOHelper
#from GaudiConf import IOHelper
#IOHelper().inputFiles([
#'/afs/ihep.ac.cn/users/l/lulanling/5T/DaVinci/LbLcpipi0/dst/2016_Lb2Sigmacpi/00073170_00000015_7.AllStreams.dst',
#'/afs/ihep.ac.cn/users/l/lulanling/5T/DaVinci/LbLcpipi0/dst/2016_Lb2Sigmacpi/00073170_00000032_7.AllStreams.dst'], clear=True)
########################

