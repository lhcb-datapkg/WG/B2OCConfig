"""
Stripping filtering file for B^\pm -> D0(*) h^\pm MC
@author Victor Daussy-Renaudin
@date   2019-08-06
Adapted from Filter_B2OC_bdh_strip28.py
"""
# Stripping version
stripping = 'stripping28r1p1'

# Use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )

# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

# Get the configuration dictionary from the database
config = strippingConfiguration(stripping)
# Get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping=config, archive=archive)

# Select my lines
MyStream = StrippingStream("bdsth.Strip")
MyLines = [
    'StrippingB2Dstar0PiDst02D0Pi0D2KSHHLLBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0Pi0D2KSHHDDBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0Pi0D2KSHHLLBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0Pi0D2KSHHDDBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0GammaD2KSHHLLBeauty2CharmLine',
    'StrippingB2Dstar0PiDst02D0GammaD2KSHHDDBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0GammaD2KSHHLLBeauty2CharmLine',
    'StrippingB2Dstar0KDst02D0GammaD2KSHHDDBeauty2CharmLine',
    'StrippingB2D0PiD2KSHHLLBeauty2CharmLine',
    'StrippingB2D0PiD2KSHHDDBeauty2CharmLine',
    'StrippingB2D0KD2KSHHLLBeauty2CharmLine',
    'StrippingB2D0KD2KSHHDDBeauty2CharmLine'
    'StrippingB2D0KD2KSHHLLPartialDBeauty2CharmLine',
    'StrippingB2D0KD2KSHHDDPartialDBeauty2CharmLine',
    'StrippingB2D0PiD2KSHHLLPartialDBeauty2CharmLine',
    'StrippingB2D0PiD2KSHHDDPartialDBeauty2CharmLine',
]

LinesToAdd = []
for stream in streams:
    for line in stream.lines:
        line._prescale = 1.0
        for MyLine in MyLines:
            if line.name() == MyLine:
                MyStream.appendLines([line])

# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()


sc = StrippingConf(Streams=[MyStream],
                   MaxCandidates=2000,
                   TESPrefix = 'Strip',
                   AcceptBadEvents=False,
                   BadEventSelection=filterBadEvents
                    )

# So that we do not get all events written out
MyStream.sequence().IgnoreFilterPassed = False

#

# Configuration of SelDSTWriter

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default': stripDSTElements(pack=enablePacking)}

SelDSTWriterConf = {
    'default': stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=False)}

dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='000000',
                         SelectionSequences=sc.activeStreams()
                         )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation='/Event/Strip/Phys/DecReports', TCK=0x41442810)

from Configurables import DaVinci
# DaVinci Configuration
DaVinci().InputType = 'DST'
DaVinci().Simulation = True
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
