## How to use this project

If you want to add a file to the WG/B2OCConfig project (e.g. a new filtering options file) follow this steps:
 * create a new branch. The branch name should contain your name and after this a meaningful name describing what changes are in there
 * add whatever files you want to add/do changes which you need
 * once you think your work is done:
   * document what you have done in the doc/release.notes file
   * create a merge request and assign it to the B2OC MC liaison
 * if you would like to have a new release of the package for grid and online usage put a short comment in your merge request
