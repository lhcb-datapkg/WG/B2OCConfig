
# Helper file to define data type for davinci

the_year      = '2017'
the_fileType  = 'MDST'
the_rootintes = "/Event/Bhadron"

from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
               RootInTES                 = the_rootintes      ,
             )

from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from b2oc_aman_b2ddhDalitz.selectB2DDh import configure_b2oc_aman_ddhDalitz_selection
dv.appendToMainSequence( configure_b2oc_aman_ddhDalitz_selection( dataYear = the_year ) )

db = CondDB  ( LatestGlobalTagByDataType = the_year )

if '__main__' == __name__ :

  print ' The year : ', the_year

