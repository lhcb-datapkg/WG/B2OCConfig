# Ganga hack to allow importing of the module
import os
importPath = os.getcwd() + "/2_NtupleMaker/DaVinciDev_v44r5/WG/B2OCConfig/python"
#importPath = "/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/WG/B2OCConfig/v1r64/python"
import sys
sys.path.insert(0,importPath)

#from GaudiConf import IOHelper
#IOHelper('ROOT').inputFiles([
#     'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.MDST/00079256/0000/00079256_00000001_6.AllStreams.mdst'] )

# Helper file to define data type for davinci

the_year      = '2015'
the_fileType  = 'MDST'
the_rootintes = "/Event/AllStreams"
the_locationRoot = ""
the_isSimOpt  = True

from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
               RootInTES                 = the_rootintes      ,
               Simulation                = True
             )

from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from b2oc_aman_b2ddhDalitz.selectB2DDh import configure_b2oc_aman_ddhDalitz_selection
dv.appendToMainSequence( configure_b2oc_aman_ddhDalitz_selection( dataYear = the_year , locationRoot = the_locationRoot , isSimulation = the_isSimOpt ) )

db = CondDB  ( LatestGlobalTagByDataType = the_year )

if '__main__' == __name__ :

  print ' The year : ', the_year
