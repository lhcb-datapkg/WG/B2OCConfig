"""
Stripping filtering file for B0s -> Ds* h MC
@author Alessandro Bertolin
@date   2020-11-26
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%40W%S%20W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

#stripping version
stripping = 'stripping21r0p2'

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

#
# Merge into one stream and run in filtering mode
#
MyStream = StrippingStream("b0s2dssth.Strip")

# Select lines by name
MyLines = ['StrippingB02DsstarPiDsstar2DGammaD2HHHBeauty2CharmLine',
           'StrippingB02DsstarKDsstar2DGammaD2HHHBeauty2CharmLine',
           'StrippingB02DsstarKMCwNoPIDDsstar2DGammaD2HHHBeauty2CharmLine'
          ]

# Remove prescale and append the line to the stream
for stream in streams:
    if 'BhadronCompleteEvent' in stream.name():
        for line in stream.lines:
            if line.name() in MyLines:
                line._prescale = 1.
                MyStream.appendLines([line])


# Configure Stripping
sc = StrippingConf(Streams = [MyStream], TESPrefix = 'Strip')
MyStream.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter, stripDSTStreamConf, stripDSTElements)

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = { 'default': stripDSTElements(pack=True) }

SelDSTWriterConf = { 'default': stripDSTStreamConf(pack=True) }

for stream in sc.activeStreams() :
       print("there is a stream called " + stream.name() + " active")
       dstWriter = SelDSTWriter("MyDSTWriter",
                                StreamConf = SelDSTWriterConf,
                                MicroDSTElements = SelDSTWriterElements,
                                OutputFileSuffix = '000000',
                                SelectionSequences = sc.activeStreams()
                                )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x39162102)

# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().DataType = "2012"
DaVinci().ProductionType = "Stripping"

DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([dstWriter.sequence()])
