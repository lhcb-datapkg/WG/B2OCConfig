"""
Stripping filtering Template for B2OC filtering requests
This example is a functional copy of  B02DHH_strip34_Filtering.py with extra dummy comments
and extra commented sections for mDST output and testing
@author Arnau Brossa Gonzalo
@date   2020-01-09
"""
##################################################################
# ONLY THESE SECTIONS NEED TO BE MODIFIED FOR STANDARD FILTERING
##################################################################

#stripping version
##################################################################
stripping='stripping29r2p3'
##################################################################

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)
 
from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"
 
#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )
#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive
 
 
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)
 
streams = buildStreams(stripping = config, archive = archive)
# Select my lines
#OUTPUT STREAM NAME
##################################################################
AllStreams = StrippingStream("b2dneutral.Strip")			 
##################################################################
linesToAdd = []

#LIST OF FILTERED LINES
##################################################################
MyLines = [ 'StrippingBeauty2XGammaExclusiveBp2DsstGammaLine',
            'StrippingBeauty2XGammaExclusiveBp2DsGammaLine',
            'StrippingBeauty2XGammaExclusiveBp2DsstPi0Line',
            'StrippingBeauty2XGammaExclusiveBp2DsPi0Line' ]
##################################################################

 
for stream in streams:
    if 'BhadronCompleteEvent' in stream.name():
        for line in stream.lines:
            line._prescale = 1.0
            if line.name() in MyLines:
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)
 
 
 
sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )
 
 
AllStreams.sequence().IgnoreFilterPassed = False # so that we do not get all events written out
 
#
 
# Configuration of SelDSTWriter
#
#STANDARD CONFIGURATION FOR FULL DST OUTPUT 
#(change this for the following commented section for mDST output)
##################################################################
enablePacking = True 
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
 
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }
 
SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }
##################################################################

'''
#STANDARD CONFIGURATION FOR MDST OUTPUT
##################################################################
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf 
                                      )

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
    }
##################################################################
'''


#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
 
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs
 
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )
 
# Add stripping TCK
from Configurables import StrippingTCK

#TCK IN THE FOLLOWING FORMAT: DaVinci version (XXrXpX) Stripping version (XXrXpX) (i.e. DaVinci 41r4p5 with Stripping28r1p1 -> TCK:41452811)
#you can find the associated DaVinci version for each stripping in: https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses
##################################################################
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x46702923)
##################################################################

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                     # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
 
# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60


