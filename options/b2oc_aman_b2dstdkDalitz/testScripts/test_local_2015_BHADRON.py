from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
     'PFN:root://grid05.lal.in2p3.fr:1094//dpm/lal.in2p3.fr/home/lhcb/LHCb/Collision15/BHADRON.MDST/00049671/0000/00049671_00000193_1.bhadron.mdst'] )
# Helper file to define data type for davinci

import sys
sys.path.insert(0,"/afs/cern.ch/work/p/pstefko/DaVinciDev_v42r6p1/WG/B2OCConfig/python")

the_year      = '2015'
the_fileType  = 'MDST'
the_rootintes = "/Event/Bhadron"

from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
               RootInTES                 = the_rootintes      ,
             )
from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from b2oc_aman_b2dstdkDalitz.selectB2DstDh import configure_b2oc_aman_dstdkDalitz_selection
dv.appendToMainSequence( configure_b2oc_aman_dstdkDalitz_selection( dataYear = the_year ) )

db = CondDB  ( LatestGlobalTagByDataType = the_year )

if '__main__' == __name__ :

  print ' The year : ', the_year

# ================================
# Read only fired events for speed
# ================================

from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
        STRIP_Code="""
        HLT_PASS('StrippingB2D0D0KBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2D0D0KD02HHD02HHBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2D0D0KD02HHD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2DstDKDstarD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2D0D0KD02K3PiD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB02DstD0KDstarD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB02DstD0KD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB02D0DKBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB02D0DKD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2DstDKBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2Dst0D0KDst02D0GammaD02HHBeauty2CharmLine') | \
        HLT_PASS('StrippingB02DstD0KBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2Dst0D0KDst02D0GammaD02HHBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2Dst0D0KDst02D0Pi0ResolvedD02HHBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2DstDPiBeauty2CharmLineDecision') | \
        HLT_PASS('StrippingB2DstDPiDstarD02K3PiBeauty2CharmLineDecision') | \
        HLT_PASS_RE('Hlt2Topo.*Decision') | \
        HLT_PASS_RE('Hlt2*IncPhi.*Decision') """
        )

# ================================
# Configure DaVinci
# ================================

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci

davinci = DaVinci (
    EventPreFilters = fltrs.filters ('Filters') ,
    EvtMax          =     10000    ,
    PrintFreq       =   1000    ,
    Lumi            =   True    ,
    TupleFile       = "B2OC_AMAN_B2DSTDSHDALITZSEL.ROOT"
    )
